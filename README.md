#EXTM3U
#PLAYLISTV: pltv-logo="" pltv-name="MEGAMAX IPTV" pltv-description="CineTelaIPTV" pltv-cover="" pltv-author="CineTelaIPTV" pltv-site="" pltv-email="iplizersouza@gmail.com" pltv-phone=""


################## Canais Abertos ##################

#EXTINF:-1 tvg-id="Agro Brasil" tvg-name="Agro Brasil" tvg-logo="https://s26.postimg.org/3yos6gz2h/agrobrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Agro Brasil
http://177.54.152.243:1935/agrobrasiltv/agrobrasiltv/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Agro Canal" tvg-name="Agro Canal" tvg-logo="https://s26.postimg.org/itddl7te1/agrocanal_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Agro Canal
http://live.sba1.com:80/hls-live/liveac/_definst_/liveevent/livestream.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Amazon Sat" tvg-name="Amazon Sat" tvg-logo="https://s26.postimg.org/mpqph81ix/amazonsat_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Amazon Sat HD
http://slrp.sambavideos.sambatech.com/liveevent/amazonsatabr1_3e9c859611a5e7fbedc785bd33c418b5/livestream3/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="Band PA" tvg-logo="https://s26.postimg.org/oub2iavft/band_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Band PA
http://stmv3.srvstm.com/rbatv12/rbatv12/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Band" tvg-name="Band SP HD" tvg-logo="https://s26.postimg.org/oub2iavft/band_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Band SP HD
http://stream.bplaymove.com:8080/arecord/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Canal Rural" tvg-name="Canal Rural" tvg-logo="https://s26.postimg.org/50yyp0pll/canalrural_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Canal Rural
http://hls-live.crur.al:80/hlshigh/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Conexão BR" tvg-name="Conexão BR" tvg-logo="https://s26.postimg.org/sf6y0yf8p/conexaobr_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Conexão BR
http://live.sba1.com:80/hls-live/livecx/_definst_/liveevent/livestream.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Globo.br" tvg-name="Globo HD" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo HD
http://stream.bplaymove.com:8080/aglobohd2/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Globo Brasília" tvg-name="Globo Brasília" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Brasília

#EXTINF:-1 tvg-id="Globo - EPTV Campinas [Brazil]" tvg-name="Globo EPTV Campinas" tvg-logo="https://s26.postimg.org/n37gjxemx/globoeptvMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo EPTV Campinas


#EXTINF:-1 tvg-id="Globo - EPTV Ribeirão Preto [Brazil]" tvg-name="Globo EPTV Ribeirão Preto" tvg-logo="https://s26.postimg.org/n37gjxemx/globoeptvMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo EPTV Ribeirão Preto


#EXTINF:-1 tvg-id="Globo Minas" tvg-name="Globo Minas" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Minas

#EXTINF:-1 tvg-id="Globo Minas" tvg-name="Globo Minas (Alternativo)" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",GLOBO MG (Alternativo)


#EXTINF:-1 tvg-id="Globo Minas" tvg-name="Globo Minas HD" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Minas HD


#EXTINF:-1 tvg-id="Globo Nordeste" tvg-name="Globo Nordeste" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Nordeste


#EXTINF:-1 tvg-id="RBS TV Porto Alegre" tvg-name="Globo RBS TV Porto Alegre" tvg-logo="https://s26.postimg.org/62okb9h15/globorbstvportoalegreMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo RBS TV Porto Alegre


#EXTINF:-1 tvg-id="Rede Amazônica Manaus" tvg-name="Globo Rede Amazônica" tvg-logo="https://s26.postimg.org/xq19pd9xl/globoredeamazonicaMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Rede Amazônica

#EXTINF:-1 tvg-id="Globo Portugal" tvg-name="Globo Portugal" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Portugal

#EXTINF:-1 tvg-id="Globo Rio" tvg-name="Globo Rio" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Rio


#EXTINF:-1 tvg-id="Globo Rio" tvg-name="Globo Rio (Alternativo)" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Rio (Alternativo)


#EXTINF:-1 tvg-id="Globo Rio" tvg-name="Globo Rio FULL HD" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Rio FULL HD


#EXTINF:-1 tvg-id="Globo Rio" tvg-name="Globo Rio HD" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo Rio HD


#EXTINF:-1 tvg-id="RPC Curitiba" tvg-name="Globo RPC Curitiba" tvg-logo="https://s26.postimg.org/xq19pdpd5/globorpctvcuritibaMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo RPC Curitiba


#EXTINF:-1 tvg-id="Globo SP" tvg-name="Globo SP" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo SP
http://aovivo.futeboltv.com:8081/live/18/chunks.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Globo SP" tvg-name="Globo SP HD" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo SP HD

#EXTINF:-1 tvg-id="Globo SP" tvg-name="Globo SP (Alternativo)" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo SP (Alternativo)


#EXTINF:-1 tvg-id="TV Anhanguera Goiânia" tvg-name="Globo TV Anhanguera" tvg-logo="https://s26.postimg.org/vy8auhdq1/globotvanhangueraMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV Anhanguera


#EXTINF:-1 tvg-id="Globo - Rede Bahia [Brazil]" tvg-name="Globo TV Bahia" tvg-logo="https://s26.postimg.org/3lct40ck9/globoredebahiaMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV Bahia


#EXTINF:-1 tvg-id="TV Centro América" tvg-name="Globo TV Centro América" tvg-logo="https://s26.postimg.org/mqg2dsedl/globotvcentroamericaMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV Centro América


#EXTINF:-1 tvg-id="TV TEM Bauru" tvg-name="Globo TV TEM Bauru" tvg-logo="https://s26.postimg.org/dintx3mqx/globotvtemMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV TEM Bauru


#EXTINF:-1 tvg-id="TV Rio Preto SJRP" tvg-name="Globo TV TEM São José do Rio Preto" tvg-logo="https://s26.postimg.org/dintx3mqx/globotvtemMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV TEM São José do Rio Preto


#EXTINF:-1 tvg-id="TV TEM Sorocaba" tvg-name="Globo TV TEM Sorocaba" tvg-logo="https://s26.postimg.org/dintx3mqx/globotvtemMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV TEM Sorocaba


#EXTINF:-1 tvg-id="TV Tribuna Santos" tvg-name="Globo TV Tribuna Santos" tvg-logo="https://s26.postimg.org/v8pii4sm1/globotvtribunaMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV Tribuna Santos


#EXTINF:-1 tvg-id="TV Vanguarda SJC" tvg-name="Globo TV Vanguarda São José dos Campos" tvg-logo="https://s26.postimg.org/v8pii581l/globotvvanguardaMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV Vanguarda São José dos Campos

#EXTINF:-1 tvg-id="TV Verdes Mares" tvg-name="Globo TV Verdes Mares" tvg-logo="https://s26.postimg.org/jjliu6osp/globotvverdesmaresMEGAMAXIPTV.jpg" group-title="Canais Abertos",Globo TV Verdes Mares

#EXTINF:-1 tvg-id="NBR" tvg-name="NBR" tvg-logo="https://s26.postimg.org/wbk9wz2t5/nbr_MEGAMAXIPTV.jpg" group-title="Canais Abertos",NBR
http://ebctveventual01-live.hls.adaptive.level3.net:80/hls-live/ebcremuxlive-ebctveventual01/_definst_/live/stream3.m3u8?MEGAMAXIPTV
 
#EXTINF:-1 tvg-id="NBR" tvg-name="NBR (Alternativo)" tvg-logo="https://s26.postimg.org/wbk9wz2t5/nbr_MEGAMAXIPTV.jpg" group-title="Canais Abertos",NBR (Alternativo)
http://ebctveventual01-live.hls.adaptive.level3.net/hls-live/ebcremuxlive-ebctveventual01/_definst_/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Novo Canal" tvg-name="Novo Canal" tvg-logo="https://s26.postimg.org/5qhr1f85l/novocanal_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Novo Canal
http://live.sba1.com:80/hls-live/livenc/_definst_/liveevent/livestream.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="RecordTV ES" tvg-logo="https://s26.postimg.org/6srxk019l/recordtv_MEGAMAXIPTV.png" group-title="Canais Abertos",RecordTV ES
http://tvvitoria2.viacast.tv/livepl/tvvitoria_2_original.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RecordTV" tvg-name="RecordTV SP HD" tvg-logo="https://s26.postimg.org/6srxk019l/recordtv_MEGAMAXIPTV.png" group-title="Canais Abertos",RecordTV SP HD
http://stream.bplaymove.com:8080/arecord/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RecordTV Internacional" tvg-name="RecordTV Internacional" tvg-logo="https://s26.postimg.org/6srxk019l/recordtv_MEGAMAXIPTV.png" group-title="Canais Abertos",RecordTV Internacional
http://api.new.livestream.com/accounts/21076186/events/6180705/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RecordTV MT" tvg-name="RecordTV MT" tvg-logo="https://s26.postimg.org/6srxk019l/recordtv_MEGAMAXIPTV.png" group-title="Canais Abertos",RecordTV MT
http://wz3.dnip.com.br:1935/tvgazetamt/tvgazetamt.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RecordTV RN" tvg-name="RecordTV RN" tvg-logo="https://s26.postimg.org/6srxk019l/recordtv_MEGAMAXIPTV.png" group-title="Canais Abertos",RecordTV RN
http://184.172.23.109:1935/live/tvtrop_natal/tvtrop_natal_aovivo/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="RecordTV SC HD" tvg-logo="https://s26.postimg.org/6srxk019l/recordtv_MEGAMAXIPTV.png" group-title="Canais Abertos",RecordTV SC HD
https://ricmais.com.br/sc/streaming/show/ric_hd720.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RBTV" tvg-name="REDE BRASIL PE" tvg-logo="https://s26.postimg.org/sf6y0zx95/redebrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Rede Brasil PE
http://rbc.directradios.com:1935/rbc/smil:rbc.smil/chunklist_w1598496838_b200000.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RBTV" tvg-name="REDE BRASIL PE HD" tvg-logo="https://s26.postimg.org/sf6y0zx95/redebrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Rede Brasil PE HD
http://rbc.directradios.com:1935/rbc/smil:rbc.smil/chunklist_w1598496838_b1200000.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RBTV" tvg-name="REDE BRASIL PE (Alternativo)" tvg-logo="https://s26.postimg.org/sf6y0zx95/redebrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Rede Brasil PE (Alternativo)
http://rbc.directradios.com:1935/rbc/smil:rbc.smil/chunklist_w306237751_b600000.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Brasil SC" tvg-name="Rede Brasil SC" tvg-logo="https://s26.postimg.org/sf6y0zx95/redebrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos", Rede Brasil SC
http://wse5.player.tv.br:1935/aovivo1/aovivo1/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Brasil SP" tvg-name="Rede Brasil SP" tvg-logo="https://s26.postimg.org/sf6y0zx95/redebrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Rede Brasil SP
http://173.236.10.10:1935/rbtv/rbtv/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Familía" tvg-name="Rede Familía" tvg-logo="https://s26.postimg.org/b1wnm5wt5/redefamilia_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Rede Familía

#EXTINF:-1 tvg-id="RedeTV! PB" tvg-name="RedeTV PB" tvg-logo="https://s26.postimg.org/j7epkc87d/redetv_MEGAMAXIPTV.png" group-title="Canais Abertos",RedeTV PB HD
https://v5.ciclano.io:1443/8064/8064/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RedeTV! RO" tvg-name="RedeTV RO" tvg-logo="https://s26.postimg.org/j7epkc87d/redetv_MEGAMAXIPTV.png" group-title="Canais Abertos",RedeTV RO

#EXTINF:-1 tvg-id="RedeTV!" tvg-name="RedeTV SP" tvg-logo="https://s26.postimg.org/j7epkc87d/redetv_MEGAMAXIPTV.png" group-title="Canais Abertos",RedeTV SP
http://evpp.mm.uol.com.br:1935/redetv1/redetv1/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT GO" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT GO
http://tvsd2.zoeweb.tv:1935/tvsd2/smil:tvsd2.smil/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT Caruaru" tvg-logo="https://s26.postimg.org/k04szvk1l/tvjornalsbt_MEGAMAXIPTV.png" group-title="Canais Abertos",SBT Caruaru
http://evpp.mm.uol.com.br:1935/ne10/ne10-tvjornal-caruaru-video-web.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT MA" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT MA
http://difusorama.zoeweb.tv:1935/z437-live/stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT Recife" tvg-logo="https://s26.postimg.org/k04szvk1l/tvjornalsbt_MEGAMAXIPTV.png" group-title="Canais Abertos",SBT Recife
http://evpp.mm.uol.com.br/ne10/ne10.smil/chunklist_w1716953599_b176000_sleng.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT Recife HD" tvg-logo="https://s26.postimg.org/k04szvk1l/tvjornalsbt_MEGAMAXIPTV.png" group-title="Canais Abertos",SBT Recife HD
http://evpp.mm.uol.com.br/ne10/ne10.smil/chunklist_w2055128690_b216000_sleng.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT Recife HD2" tvg-logo="https://s26.postimg.org/k04szvk1l/tvjornalsbt_MEGAMAXIPTV.png" group-title="Canais Abertos",SBT Recife HD*
http://evpp.mm.uol.com.br/ne10/ne10.smil/chunklist_w1846566125_b216000_sleng.m3u8

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT RJ" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT RJ HD


#EXTINF:-1 tvg-id="SBT" tvg-name="SBT PI HD" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT PI HD
http://192.99.46.182:1935/pioneira-tv3/pioneira-tv3/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT SC" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT SC
https://slbps-sambavideos.akamaized.net/liveevent/livesc_7669e5c05e7b20455acc3bdec60c7f23/livestream/chunklist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT SP" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT SP
https://gbbrslbps-sambatech.akamaized.net/liveevent/sbtabr_8fcdc5f0f8df8d4de56b22a2c6660470/livestream2/chunklist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SBT" tvg-name="SBT SP Interior" tvg-logo="https://s26.postimg.org/tu8ipq621/sbt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",SBT SP (Interior)
http://srv6.zoeweb.tv:1935/z343-live/stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SescTV" tvg-name="Sesc TV HD" tvg-logo="https://s26.postimg.org/56q5fbex5/sesctv_MEGAMAXIPTV.png" group-title="Canais Abertos",SescTV HD
http://api.new.livestream.com/accounts/16991778/events/6670101/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Terra Viva" tvg-name="Terra Viva" tvg-logo="https://s26.postimg.org/3yos6jby1/terraviva_MEGAMAXIPTV.jpg" group-title="Canais Abertos",Terra Viva
http://evpp.mm.uol.com.br:1935/band/terraviva/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Aberta" tvg-name="TV Aberta SP" tvg-logo="https://s26.postimg.org/sf6y11mzd/tvabertasaopaulo_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Aberta SP
http://live.tvaberta.tv.br:1935/tvabertalive/tvabertalive/chunklist_w128464446.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Brasil" tvg-name="TV Brasil" tvg-logo="https://s26.postimg.org/7v242kmnt/tvbrasil_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Brasil

#EXTINF:-1 tvg-id="Cultura BA" tvg-name="TV Cultura BA" tvg-logo="https://s26.postimg.org/kz7of5z95/cultura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Cultura BA HD
http://wz-brasil1.comets.com.br:1935/tvbaiana/aovivo/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TCM Canal 10" tvg-name="" tvg-logo="https://s26.postimg.org/le118p3h5/tcmcanal10mossoro_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TCM Canal 10
http://cw.tcm10.com.br/low/canal10.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Cultura MG" tvg-name="TV Cultura MG" tvg-logo="https://s26.postimg.org/kz7of5z95/cultura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Cultura MG
http://rtmp.cdn.upx.net.br:1935/00084/myStream.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Cultura PA" tvg-name="TV Cultura PA" tvg-logo="https://s26.postimg.org/kz7of5z95/cultura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Cultura PA FULL HD
http://str.portalcultura.com.br:80/funtelpa/tv_funtelpa/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Cultura PB" tvg-name="TV Cultura PB" tvg-logo="https://s26.postimg.org/kz7of5z95/cultura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Cultura PB


#EXTINF:-1 tvg-id="Cultura PE" tvg-name="TV Cultura PE" tvg-logo="https://s26.postimg.org/kz7of5z95/cultura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Cultura PE HD


#EXTINF:-1 tvg-id="Cultura PR" tvg-name="Cultura PR" tvg-logo="https://s26.postimg.org/kz7of5z95/cultura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Cultura PR
http://200.189.113.201/hls/tve.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Câmara" tvg-name="TV Câmara" tvg-logo="https://s26.postimg.org/k9ow2wlvt/tvcamara_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Câmara
http://stream2.camara.leg.br:8134/tv1/manifest.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Diário" tvg-name="TV Diário" tvg-logo="https://s26.postimg.org/4bg6crzdl/tvdiario_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Diário
http://slrp.sambavideos.sambatech.com/liveevent/tvdiario_7a683b067e5eee5c8d45e1e1883f69b9/livestream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Escola" tvg-name="TV Escola" tvg-logo="https://s26.postimg.org/63957pdll/tvescola_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Escola
http://slrp.sambavideos.sambatech.com/liveevent/acerpTvEscolaABR_1f9a5d00db56b3c3020b6ac3dd693e12/livestream2/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Escola" tvg-name="TV Escola" tvg-logo="https://s26.postimg.org/63957pdll/tvescola_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Escola HD
http://slrp.sambavideos.sambatech.com/liveevent/acerpTvEscolaABR_1f9a5d00db56b3c3020b6ac3dd693e12/livestream3/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Futura RS" tvg-name="Futura RS" tvg-logo="https://s26.postimg.org/6srxjxw3t/futura_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Futura RS
http://cdn.srv.br:8088/UCSTV/UCSTV/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Gazeta ES" tvg-name="TV Gazeta ES" tvg-logo="https://s26.postimg.org/z5nfais5l/tvgazetaes_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Gazeta
http://api.new.livestream.com/accounts/5381476/events/2395418/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Ines" tvg-name="TV Ines" tvg-logo="https://s26.postimg.org/u6zww03s9/tvines_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Inês
http://slrp.sambavideos.sambatech.com/liveevent/acerpTvINESABR_d360fbbfbf3e5e59a31cf89ec45eb3dc/livestream2/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Maceió" tvg-name="Tv Maceió" tvg-logo="https://s26.postimg.org/6srxk3909/tvmaceio_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Maceió
http://173.236.10.10:1935/toptv/toptv/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Medicina e Saúde" tvg-name="Tv Medicina e Saúde" tvg-logo="https://s26.postimg.org/yg4my6erd/tvmedicina_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Medicina e Saúde
http://rtmp.cdn.upx.net.br:1935/7dcb/myStream.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Meio Norte" tvg-name="TV Meio Norte" tvg-logo="https://s26.postimg.org/xdugfkbcp/redemeionorte_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Meio Norte
http://api.new.livestream.com/accounts/3332377/events/2958396/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Pajuçara" tvg-name="TV Pajuçara" tvg-logo="https://s26.postimg.org/l3n012qt5/tvpajuçaraMEGAMAXIPTV.jpg" group-title="Canais Abertos",TV Pajuçara
http://slrp.sambavideos.sambatech.com/liveevent/pajucara1_7fbed8aac5d5d915877e6ec61e3cf0db/livestream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Urbana" tvg-name="TV Urbana" tvg-logo="https://s26.postimg.org/acdv9wr5l/tvu_MEGAMAXIPTV.png" group-title="Canais Abertos",TV Urbana
http://srv9.zoeweb.tv:1935/zw923/stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TVE Bahia" tvg-name="TVE Bahia" tvg-logo="https://s26.postimg.org/i54j1tzop/tvebahia_MEGAMAXIPTV.png" group-title="Canais Abertos",TVE Bahia

#EXTINF:-1 tvg-id="TVE RS" tvg-name="TVE RS" tvg-logo="https://s26.postimg.org/hflqph6ux/tvers_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TVE RS
http://streaming.procergs.com.br:1935/tve/stve/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TVT" tvg-name="TVT" tvg-logo="https://s26.postimg.org/wobo3a349/tvt_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TVT

#EXTINF:-1 tvg-id="TVU Recife" tvg-name="TVU Recife" tvg-logo="https://s26.postimg.org/nriyzq51l/tvurecife_MEGAMAXIPTV.jpg" group-title="Canais Abertos",TVU Recife HD
http://150.161.93.173/hls/720/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV UFG" tvg-name="TV UFG" tvg-logo="https://s26.postimg.org/ey9zi94yx/tvufg_MEGAMAXIPTV.png" group-title="Canais Abertos",TV UFG


################## Religiosos ##################

#EXTINF:-1 tvg-id="" tvg-name="ADINET TV" tvg-logo="" group-title="Religiosos",ADINET TV

#EXTINF:-1 tvg-id="Boa Vontade TV" tvg-name="Boa Vontade TV" tvg-logo="https://s26.postimg.org/md0ownart/boavontadetv_MEGAMAXIPTV.jpg" group-title="Religiosos",Boa Vontade TV FULL HD
http://api.new.livestream.com/accounts/15107153/events/4338771/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Canção Nova" tvg-name="Canção Nova" tvg-logo="https://s26.postimg.org/hr4kobrtl/can_onova_MEGAMAXIPTV.jpg" group-title="Religiosos",Canção Nova
http://cancaonova-push-live.hls.adaptive.level3.net/hls-live/cancaonova-channel01/_definst_/live/stream1.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Canção Nova" tvg-name="Canção Nova HD" tvg-logo="https://s26.postimg.org/hr4kobrtl/can_onova_MEGAMAXIPTV.jpg" group-title="Religiosos",Canção Nova HD
http://tvajuhls-lh.akamaihd.net:80/i/tvdesk_1@147040/index_1080_av-p.m3u8

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/qywt55gx5/canalcjcMEGAMAXIPTV.jpg" group-title="Religiosos",CJC TV
http://flash1.crossdigital.com.br:1935/ponto85/ongrace/playlist.m3u8?

#EXTINF:-1 tvg-id="Gospel Movies Television" tvg-name="Gospel Movies Television" tvg-logo="https://s26.postimg.org/sqps01kuh/gospelmoviestelevisionMEGAMAXIPTV.jpg" group-title="Religiosos",Gospel Movies Television
http://stmv2.srvstm.com:1935/gospelf/gospelf/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Impd TV" tvg-name="Impd TV" tvg-logo="https://s26.postimg.org/4a7m5f77d/impdtvMEGAMAXIPTV.png" group-title="Religiosos",IMPD TV
https://58a4464faef53.streamlock.net/impd/ngrp:impd_all/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Iurd TV" tvg-name="Iurd TV" tvg-logo="https://s26.postimg.org/xpdaefr6h/iurdtvMEGAMAXIPTV.png" group-title="Religiosos",IURD TV
http://iuhls-lh.akamaihd-staging.net/i/utvhls1_1@336317/master.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Novo Tempo" tvg-name="TV Novo Tempo" tvg-logo="https://s26.postimg.org/goue5xtmh/tvnovotempoMEGAMAXIPTV.jpg" group-title="Religiosos",Novo Tempo SP
http://stream.novotempo.com:1935/tv/tvnovotempo.stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Portal Gospel TV" tvg-name="Portal Gospel TV" tvg-logo="https://s26.postimg.org/jixjj7o15/portalgospel24horas.png" group-title="Religiosos",Portal Gospel 24H
http://s1.wsepanel.com:1935/lukfilm/lukfilm/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rbi" tvg-name="Rbi" tvg-logo="https://s26.postimg.org/o4tnrlzah/rbitvMEGAMAXIPTV.jpg" group-title="Religiosos",RBI TV

#EXTINF:-1 tvg-id="Rede Gospel" tvg-name="Rede Gospel" tvg-logo="https://s26.postimg.org/wn33vxqdl/redegospelMEGAMAXIPTV.png" group-title="Religiosos",Rede Gospel
http://media.igospel.com.br:1935/live/redegospel/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Gênesis" tvg-name="Rede Gênesis" tvg-logo="https://s26.postimg.org/bdfhl14x5/redegenesisMEGAMAXIPTV.png" group-title="Religiosos",Rede Gênesis
http://srv3.zoeweb.tv:1935/z474-live/aovivo/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Super" tvg-name="Rede Super" tvg-logo="https://s26.postimg.org/csh29s8l5/redesuperMEGAMAXIPTV.jpg" group-title="Religiosos",Rede Super
http://api.new.livestream.com/accounts/10205943/events/3429501/live.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Século 21" tvg-name="Rede Século 21" tvg-logo="https://s26.postimg.org/vxkbjlsex/redeseculo21MEGAMAXIPTV.png" group-title="Religiosos",Rede Século 21
http://tvseculo21-lh.akamaihd.net/i/tvseculo_1@16110/master.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Rede Vida" tvg-name="Rede Vida" tvg-logo="https://s26.postimg.org/e7imyj4jd/redevidaMEGAMAXIPTV.jpg" group-title="Religiosos",Rede Vida
http://rtmp.cdn.upx.net.br:1935/74cc/myStream.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Show da Fé" tvg-name="Show da Fé" tvg-logo="https://s26.postimg.org/5chso0v6h/canalshowdafe_MEGAMAXIPTV.png" group-title="Religiosos",Show da Fé
http://nossatvhls02-lh.akamaihd.net/i/nossatv_hls@381916/master.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Aparecida" tvg-name="Tv Aparecida" tvg-logo="https://s26.postimg.org/tu8iprvs9/tvaparecida_MEGAMAXIPTV.jpg" group-title="Religiosos",TV Aparecida
http://caikron.com.br:1935/tvaparecida/tvaparecida.stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Evangelizar" tvg-name="Tv Evangelizar" tvg-logo="https://s26.postimg.org/bq6vrafi1/tvevangelizarMEGAMAXIPTV.jpg" group-title="Religiosos",TV Evangelizar
http://tvevangelizar20.crossdigital.com.br:1935/evangelizar/tv/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="Tv Feliz" tvg-logo="https://s26.postimg.org/ignd0txjt/tvfelizMEGAMAXIPTV.jpg" group-title="Religiosos",TV Feliz
https://59f2354c05961.streamlock.net:1443/tvfelizrn/_definst_/tvfelizrn/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Terceiro Anjo" tvg-name="Tv Terceiro Anjo" tvg-logo="https://s26.postimg.org/6dogwkki1/tvterceiroanjo_MEGAMAXIPTV.jpg" group-title="Religiosos",TV Terceiro Anjo
http://streamer1.streamhost.org:1935/salive/GMI3anjoh/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Tv Verdade" tvg-name="Tv Verdade" tvg-logo="https://s26.postimg.org/4a7m5lueh/tvverdadeMEGAMAXIPTV.jpg" group-title="Religiosos",TV Verdade
http://ocatv.mcgi.me:1935/public/247_br/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Vem e Vê TV" tvg-name="Vem e Vê TV" tvg-logo="https://s26.postimg.org/rbo7bd9hl/vemevetvMEGAMAXIPTV.jpg" group-title="Religiosos",Vem e Vê TV 1
http://srv-vvtv.sisdera.com:1935/live/mp4:vvtv2/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Vem e Vê TV 2" tvg-name="Vem e Vê TV 2" tvg-logo="https://s26.postimg.org/rbo7bd9hl/vemevetvMEGAMAXIPTV.jpg" group-title="Religiosos",Vem e Vê TV 2
http://srv-vvtv.sisdera.com:1935/live/mp4:vvtv022/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Vem e Vê TV 3" tvg-name="Vem e Vê TV 3" tvg-logo="https://s26.postimg.org/rbo7bd9hl/vemevetvMEGAMAXIPTV.jpg" group-title="Religiosos",Vem e Vê TV 3
http://srv-vvtv.sisdera.com:1935/live/mp4:vvtv032/playlist.m3u8?MEGAMAXIPTV

################## Variedades e Músicas ##################

#EXTINF:-1 tvg-id="A&E" tvg-name="A&E" tvg-logo="https://s26.postimg.org/7j02t9u6x/a&eMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",A&E


#EXTINF:-1 tvg-id="Arte 1" tvg-name="Arte 1" tvg-logo="https://s26.postimg.org/62okb01ix/arte1MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Arte 1

#EXTINF:-1 tvg-id="Bis" tvg-name="Bis" tvg-logo="https://s26.postimg.org/abtad6rxl/bisMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Bis

#EXTINF:-1 tvg-id="Canal Brasil" tvg-name="Canal Brasil" tvg-logo="https://s26.postimg.org/m0xa170wp/canalbrasilMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Canal Brasil


#EXTINF:-1 tvg-id="" tvg-name="Cine +" tvg-logo="https://s26.postimg.org/ex1fatcrt/cinemais_MEGAMAXIPTV.png" group-title="Variedades e Músicas",Cine +
http://189.86.89.116:80/hls-live/livecm/_definst_/liveevent/livestream.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="Classique TV" tvg-logo="https://s26.postimg.org/xpdaeewbd/classiquetv_MEGAMAXIPTV.png" group-title="Variedades e Músicas",Classique TV
http://stmv2.euroti.com.br:1935/classique/classique/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Comedy Central" tvg-name="Comedy Central" tvg-logo="https://s26.postimg.org/dvf8332e1/comedycentralMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Comedy Central
http://stream.bplaymove.com:80/fcomedycentral/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Comedy Central" tvg-name="Comedy Central HD" tvg-logo="https://s26.postimg.org/dvf8332e1/comedycentralMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Comedy Central HD
http://stream.bplaymove.com:80/fcomedycentralhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Curta!" tvg-name="Curta" tvg-logo="https://s26.postimg.org/7hq4zu57t/curta!MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Curta


#EXTINF:-1 tvg-id="Discovery Home & Health" tvg-name="Discovery Home & Health" tvg-logo="https://s26.postimg.org/5px64y1ah/discoveryhome&healthMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Discovery Home & Health


#EXTINF:-1 tvg-id="DW-TV" tvg-name="DW (America)" tvg-logo="https://s26.postimg.org/qa203gz21/dwamericaMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",DW (America)
http://dwstream3-lh.akamaihd.net/i/dwstream3_live@124409/index_1_av-p.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="E!" tvg-name="E!" tvg-logo="https://s26.postimg.org/4nmzmbig9/canale!MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",E!


#EXTINF:-1 tvg-id="Fish TV" tvg-name="Fish TV" tvg-logo="https://s26.postimg.org/rbo7b4ouh/fishtv_MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Fish TV
http://stream.bplaymove.com:80/dfishtv/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Fish TV" tvg-name="Fish TV HD" tvg-logo="https://s26.postimg.org/rbo7b4ouh/fishtv_MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Fish TV HD

#EXTINF:-1 tvg-id="Food Network" tvg-name="Food Network" tvg-logo="https://s26.postimg.org/d5wfqtp0p/foodnetworkMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Food Network


#EXTINF:-1 tvg-id="FoxLife" tvg-name="Fox Life" tvg-logo="https://s26.postimg.org/5px650tll/foxlifeMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Fox Life


#EXTINF:-1 tvg-id="" tvg-name="G2P" tvg-logo="https://s26.postimg.org/8jcc7q7xl/g2pMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",G2P
http://srv3.zoeweb.tv:1935/z474-live/aovivo/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="GNT" tvg-name="GNT" tvg-logo="https://s26.postimg.org/63957lazt/gnt_MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",GNT


#EXTINF:-1 tvg-id="Heart TV" tvg-name="Heart TV" tvg-logo="https://s26.postimg.org/620l0ggbd/hearttvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Heart TV

#EXTINF:-1 tvg-id="" tvg-name="H!T Music Channel" tvg-logo="https://s26.postimg.org/3xg7ze9jt/hitmusicchannelMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",H!T Music Channel
http://1mstream.digicable.hu/hitmusic/hitmusic.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HIT TV" tvg-name="HIT TV" tvg-logo="https://s26.postimg.org/4zqehxcxl/hittvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",HIT TV
http://kissfm-cires21-video.secure.footprint.net/hittv/master.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Lifetime" tvg-name="Lifetime" tvg-logo="https://s26.postimg.org/62okbdjmx/lifetimeMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Lifetime


#EXTINF:-1 tvg-id="Mais GloboSat" tvg-name="Mais GloboSat" tvg-logo="https://s26.postimg.org/ct51kteih/maisglobosatMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Mais GloboSat


#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/p73ua2i2x/musicboxgooldMEGAMAXIPTV.png" group-title="Variedades e Músicas",Music Box Gold
http://musicbox.cdnvideo.ru:1935/musicbox/musicboxtv.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="MTV" tvg-name="MTV" tvg-logo="https://s26.postimg.org/ht2hs6zeh/mtvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",MTV
http://stream.bplaymove.com:80/vmtv/video.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="MTV" tvg-name="MTV HD" tvg-logo="https://s26.postimg.org/ht2hs6zeh/mtvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",MTV HD
http://stream.bplaymove.com:80/vmtvhd/video.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Multishow" tvg-name="Multishow" tvg-logo="https://s26.postimg.org/y42loirbt/multishowMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Multishow
http://stream.bplaymove.com:8080/vmultishow/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Multishow" tvg-name="Multishow HD" tvg-logo="https://s26.postimg.org/y42loirbt/multishowMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Multishow HD
http://stream.bplaymove.com:8080/vmultishowhd/index.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="Music Box Brazil" tvg-name="Music Box Brasil" tvg-logo="https://s26.postimg.org/9at1nvg15/musicboxbrasilMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Music Box Brasil


#EXTINF:-1 tvg-id="NHK World Premium" tvg-name="NHK HD" tvg-logo="https://s26.postimg.org/gqsb9oqvt/nhkMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",NHK HD
http://nhkwtvglobal-i.akamaihd.net/hls/live/263941/nhkwtvglobal/index_1180.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Off" tvg-name="Off HD" tvg-logo="https://s26.postimg.org/6ffyh8h8p/canaloffMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Off HD


#EXTINF:-1 tvg-id="PlayTV" tvg-name="Play TV" tvg-logo="https://s26.postimg.org/6thagne55/playtvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Play TV


#EXTINF:-1 tvg-id="Polishop TV" tvg-name="Polishop TV" tvg-logo="https://s26.postimg.org/n4heczds9/polishopMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Polishop TV


#EXTINF:-1 tvg-id="Prime Box Brazil" tvg-name="Prime Box Brasil" tvg-logo="https://s26.postimg.org/4owxfkry1/primeboxbrasilMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Prime Box Brasil


#EXTINF:-1 tvg-id="Rai Italia" tvg-name="RAI" tvg-logo="https://s26.postimg.org/nrld01h49/rai_MEGAMAXIPTV.jpg" group-title="Variedades e Músicas",RAI
http://b2everyrai-lh.akamaihd.net/i/rainews_1@179616/master.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="RCI" tvg-name="RCI" tvg-logo="https://s26.postimg.org/vzi8niaah/rciMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",RCI


#EXTINF:-1 tvg-id="SIC Internacional" tvg-name="SIC Internacional" tvg-logo="https://s26.postimg.org/e9gk2hc55/sicinternetionalMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",SIC Internacional


#EXTINF:-1 tvg-id="TLC" tvg-name="TLC" tvg-logo="https://s26.postimg.org/ge0x3o0y1/tlcMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",TLC


#EXTINF:-1 tvg-id="" tvg-name="Top TV" tvg-logo="https://s26.postimg.org/y24okru2h/toptvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Top TV
http://sateg.cdnseguro.com/toptv/toptv/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TruTV" tvg-name="TRU TV" tvg-logo="https://s26.postimg.org/foi4rbv9l/trutvMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",TRU TV


#EXTINF:-1 tvg-id="TV Justiça" tvg-name="TV Justiça" tvg-logo="https://s26.postimg.org/51oblxhzd/tvjustiçaMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",TV Justiça


#EXTINF:-1 tvg-id="TV5Monde Bresil" tvg-name="TV5 Monde" tvg-logo="https://s26.postimg.org/5efps3sjd/tv5mondeMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",TV5 Monde


#EXTINF:-1 tvg-id="VH1 MegaHits" tvg-name="VH1 MegaHits" tvg-logo="https://s26.postimg.org/tuxvmlo55/vh1megahitsMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",VH1 MegaHits

#EXTINF:-1 tvg-id="Viva" tvg-name="Viva" tvg-logo="https://s26.postimg.org/vmquhif7t/vivaMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Viva
http://stream.bplaymove.com/vviva/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Viva" tvg-name="Viva HD" tvg-logo="https://s26.postimg.org/vmquhif7t/vivaMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Viva HD
http://stream.bplaymove.com/vvivahd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Woohoo" tvg-name="Woohoo" tvg-logo="https://s26.postimg.org/apumcuwmh/woohooMEGAMAXIPTV.jpg" group-title="Variedades e Músicas",Woohoo


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Variedades e Músicas",

################## Esportes ##################

#EXTINF:-1 tvg-id="" tvg-name="All Sports" tvg-logo="https://s26.postimg.org/cfoahygs9/allsports_MEGAMAXIPTV.jpg" group-title="Esportes",All Sports
http://173.236.10.10:1935/dgrau/dgrau/playlist.m3u8

#EXTINF:-1 tvg-id="BandSports" tvg-name="Band Sports" tvg-logo="https://s26.postimg.org/mqg2diyvd/bandsportsMEGAMAXIPTV.jpg" group-title="Esportes",Band Sports
http://aovivo.futeboltv.com:8081/live/2/chunks.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="BandSports" tvg-name="Band Sports HD" tvg-logo="https://s26.postimg.org/mqg2diyvd/bandsportsMEGAMAXIPTV.jpg" group-title="Esportes",Band Sports HD
http://stream.bplaymove.com:8080/ebandsporthd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Combate" tvg-name="Combate" tvg-logo="https://s26.postimg.org/ihbcbf8hl/combateMEGAMAXIPTV.jpg" group-title="Esportes",Combate
http://stream.bplaymove.com:80/pcombate/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Combate" tvg-name="Combate HD" tvg-logo="https://s26.postimg.org/ihbcbf8hl/combateMEGAMAXIPTV.jpg" group-title="Esportes",Combate HD
http://stream.bplaymove.com:80/pcombatehd/index.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="ESPN" tvg-name="ESPN" tvg-logo="https://s26.postimg.org/baaiuzgg9/espnMEGAMAXIPTV.jpg" group-title="Esportes",ESPN
http://aovivo.futeboltv.com:8081/live/16/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="ESPN" tvg-name="ESPN HD" tvg-logo="https://s26.postimg.org/baaiuzgg9/espnMEGAMAXIPTV.jpg" group-title="Esportes",ESPN HD
http://stream.bplaymove.com:8080/eespnhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="ESPN Brasil" tvg-name="ESPN Brasil" tvg-logo="https://s26.postimg.org/wnr36qr3d/espnbrasilMEGAMAXIPTV.jpg" group-title="Esportes",ESPN Brasil HD
http://aovivo.futeboltv.com:8081/live/19/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="ESPN +" tvg-name="ESPN +" tvg-logo="https://s26.postimg.org/6ffyhcrk9/espn_MEGAMAXIPTV.jpg" group-title="Esportes",ESPN +


#EXTINF:-1 tvg-id="Esporte Interativo" tvg-name="Esporte Interativo" tvg-logo="https://s26.postimg.org/xd9vj472h/esporteinterativoMEGAMAXIPTV.jpg" group-title="Esportes",Esporte Interativo
http://aovivo.futeboltv.com:8081/live/8/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="Esporte Interativo" tvg-name="Esporte Interativo HD" tvg-logo="https://s26.postimg.org/xd9vj472h/esporteinterativoMEGAMAXIPTV.jpg" group-title="Esportes",Esporte Interativo HD
http://stream.bplaymove.com:8080/eesporteinterativohd/index.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="Esporte Interativo BR" tvg-name="Esporte Interativo" tvg-logo="https://s26.postimg.org/xd9vj472h/esporteinterativoMEGAMAXIPTV.jpg" group-title="Esportes",Esporte Interativo BR HD
http://d2xuvs5rzrvjs7.cloudfront.net/live-stream-secure/58121908685da2dd08de7ec5/publish/media_2500.m3u8?es=vrzn-ei-ee.cdn.mdstrm.com&proto=http
#EXTINF:-1 tvg-id="Esporte Interativo 2" tvg-name="Esporte Interativo 2" tvg-logo="https://s26.postimg.org/mdoo7i8xl/esporteinterativo2MEGAMAXIPTV.jpg" group-title="Esportes",Esporte Interativo 2 HD
http://d2xuvs5rzrvjs7.cloudfront.net/live-stream-secure/5801261189b415d108f9603c/publish/media_2500.m3u8?es=vrzn-ei-ee.cdn.mdstrm.com&proto=http

#EXTINF:-1 tvg-id="FoxSports" tvg-name="Fox Sports" tvg-logo="https://s26.postimg.org/mdoo7k6dl/foxsportsMEGAMAXIPTV.jpg" group-title="Esportes",Fox Sports
http://aovivo.futeboltv.com:8081/live/3/chunks.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="FoxSports" tvg-name="Fox Sports HD" tvg-logo="https://s26.postimg.org/mdoo7k6dl/foxsportsMEGAMAXIPTV.jpg" group-title="Esportes",Fox Sports HD
http://stream.bplaymove.com:8080/efoxsporthd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="FoxSports 2" tvg-name="Fox Sports 2" tvg-logo="https://s26.postimg.org/50edsovmx/foxsports2MEGAMAXIPTV.jpg" group-title="Esportes",Fox Sports 2
http://aovivo.futeboltv.com:8081/live/13/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Horse Brasil" tvg-name="Horse Brasil" tvg-logo="https://s26.postimg.org/uiiqurwft/horsebrasilMEGAMAXIPTV.png" group-title="Esportes",Horse Brasil HD
http://490636978.r.worldcdn.net/490636978/_definst_//horsebrasil1/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="NBA TV" tvg-name="NBA TV" tvg-logo="https://s26.postimg.org/9ydwwa195/nbatvMEGAMAXIPTV.png" group-title="Esportes",NBA TV HD
http://31.204.150.12:8080/edge/4304/tracks-v1a1/index.m3u8?token=17.18.1487.6d44eab61dcf5b1f0e6fa8f6595ee1bc.1.1.1.641.0.0.71875765cf0ee5df30360b6487a7b6ec

#EXTINF:-1 tvg-id="Premiere Clube" tvg-name="Premiere Clubes" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere Clubes
http://stream.bplaymove.com:80/ppremiereclubs/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere Clube" tvg-name="Premiere Clubes HD" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere Clubes HD
http://stream.bplaymove.com:80/ppremiereclubshd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere Clube" tvg-name="Premiere Clubes HD" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere Clubes HD


#EXTINF:-1 tvg-id="Premiere 2" tvg-name="Premiere 2" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 2
http://stream.bplaymove.com:80/ppremiere2/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 3" tvg-name="Premiere 3" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 3
http://stream.bplaymove.com:80/ppremiere3/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 4" tvg-name="Premiere 4" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 4
http://stream.bplaymove.com:80/ppremiere4/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 5" tvg-name="Premiere 5" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 5
http://stream.bplaymove.com:80/ppremiere5/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 6" tvg-name="Premiere 6" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 6
http://stream.bplaymove.com:80/ppremiere6/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 7" tvg-name="Premiere 7" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 7
http://stream.bplaymove.com:80/ppremiere7/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 8" tvg-name="Premiere 8" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 8
http://stream.bplaymove.com:80/ppremiere8/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Premiere 9" tvg-name="Premiere 9" tvg-logo="https://s26.postimg.org/3ze537495/pfcMEGAMAXIPTV.jpg" group-title="Esportes",Premiere 9
http://stream.bplaymove.com:80/ppremiere9/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Real Madrid TV" tvg-name="Real Madrid TV" tvg-logo="https://s26.postimg.org/y24okk4ah/realmadridtvMEGAMAXIPTVBR.jpg" group-title="Esportes",Real Madrid TV
http://rmtvlive-lh.akamaihd.net/i/rmtv_1@154306/master.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SporTV" tvg-name="SporTV" tvg-logo="https://s26.postimg.org/6thagpr0p/sportvMEGAMAXIPTV.jpg" group-title="Esportes",SporTV
http://aovivo.futeboltv.com:8081/live/20/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SporTV" tvg-name="SporTV HD" tvg-logo="https://s26.postimg.org/6thagpr0p/sportvMEGAMAXIPTV.jpg" group-title="Esportes",SporTV HD
http://stream.bplaymove.com:80/esportvhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SporTV 2" tvg-name="SporTV 2" tvg-logo="https://s26.postimg.org/o6rkvjz6h/sportv2MEGAMAXIPTV.jpg" group-title="Esportes",SporTV 2
http://aovivo.futeboltv.com:8081/live/15/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SporTV 2 HD" tvg-name="SporTV 2" tvg-logo="https://s26.postimg.org/o6rkvjz6h/sportv2MEGAMAXIPTV.jpg" group-title="Esportes",SporTV 2 HD
http://stream.bplaymove.com:80/esportv2hd/index.m3u8?MEGAMAXIPTV?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SporTV 3" tvg-name="SporTV 3" tvg-logo="https://s26.postimg.org/d76djygh5/sportv3MEGAMAXIPTV.jpg" group-title="Esportes",SporTV 3
http://aovivo.futeboltv.com:8081/live/7/chunks.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="SporTV 3" tvg-name="SporTV 3" tvg-logo="https://s26.postimg.org/d76djygh5/sportv3MEGAMAXIPTV.jpg" group-title="Esportes",SporTV 3 HD
http://stream.bplaymove.com:80/esportv3hd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Esportes",
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Esportes",

################## Futebol TV ##################

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 1
http://aovivo.futeboltv.com:8081/live/2/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 2
http://aovivo.futeboltv.com:8081/live/3/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 3
http://aovivo.futeboltv.com:8081/live/4/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 4
http://aovivo.futeboltv.com:8081/live/8/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 5
http://aovivo.futeboltv.com:8081/live/15/chunks.m3u8?MEGAMAXIPTV 
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 6
http://aovivo.futeboltv.com:8081/live/16/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/mitaw298p/bigbrotherbrasil_MEGAMAXIPTV.jpg" group-title="Futebol TV", BBB 18 (FTV)
http://aovivo.futeboltv.com:8081/live/17/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/p8cehchy1/globo_MEGAMAXIPTV.jpg" group-title="Futebol TV", Globo SP (FTV)
http://aovivo.futeboltv.com:8081/live/18/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/baaiuzgg9/espn_MEGAMAXIPTV.jpg" group-title="Futebol TV", ESPN (FTV)
http://aovivo.futeboltv.com:8081/live/19/chunks.m3u8?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/rg6vhlqrt/futeboltvMEGAMAXIPTV.jpg" group-title="Futebol TV", Futebol TV 10
http://aovivo.futeboltv.com:8081/live/19/chunks.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Esportes",
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Esportes",
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Esportes",
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Esportes",

################## Documentários ##################

#EXTINF:-1 tvg-id="Animal Planet" tvg-name="Animal Planet" tvg-logo="https://s26.postimg.org/f9rfveye1/animalplanet_MEGAMAXIPTV.jpg" group-title="Documentários",Animal Planet
http://stream.bplaymove.com:8080/danimalplanet/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Animal Planet" tvg-name="Animal Planet HD" tvg-logo="https://s26.postimg.org/f9rfveye1/animalplanet_MEGAMAXIPTV.jpg" group-title="Documentários",Animal Planet HD
http://stream.bplaymove.com:8080/danimalplanethd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Discovery Channel" tvg-name="Discovery Channel" tvg-logo="https://s26.postimg.org/jwcx064ft/discoverychannelMEGAMAXIPTV.jpg" group-title="Documentários",Discovery Channel
http://stream.bplaymove.com:8080/ddiscoverychannel/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Discovery Channel" tvg-name="Discovery Channel HD" tvg-logo="https://s26.postimg.org/jwcx064ft/discoverychannelMEGAMAXIPTV.jpg" group-title="Documentários",Discovery Channel HD
http://stream.bplaymove.com:8080/ddiscoverychannelhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Discovery Civilization" tvg-name="Discovery Civilization" tvg-logo="https://s26.postimg.org/v7g17phw9/discoverycivilization_MEGAMAXIPTV.jpg" group-title="Documentários",Discovery Civilization


#EXTINF:-1 tvg-id="Discovery Science" tvg-name="Discovery Science" tvg-logo="https://s26.postimg.org/y2snveuqh/discoveryscienceMEGAMAXIPTV.jpg" group-title="Documentários",Discovery Science


#EXTINF:-1 tvg-id="Discovery Theater" tvg-name="Discovery Theater" tvg-logo="https://s26.postimg.org/bquv213c9/discoverytheaterMEGAMAXIPTV.jpg" group-title="Documentários",Discovery Theater


#EXTINF:-1 tvg-id="Discovery Turbo" tvg-name="Discovery Turbo" tvg-logo="https://s26.postimg.org/e86m9auyh/discoveryturboMEGAMAXIPTV.jpg" group-title="Documentários",Discovery Turbo


#EXTINF:-1 tvg-id="Discovery World" tvg-name="Discovery World" tvg-logo="https://s26.postimg.org/jjliu146h/discoveryworldMEGAMAXIPTV.jpg" group-title="Documentários",Discovery World


#EXTINF:-1 tvg-id="H2" tvg-name="H2" tvg-logo="https://s26.postimg.org/wnr36vwa1/h2MEGAMAXIPTV.jpg" group-title="Documentários",H2


#EXTINF:-1 tvg-id="History" tvg-name="History" tvg-logo="https://s26.postimg.org/p91re6i0p/thehistorychannelMEGAMAXIPTV.jpg" group-title="Documentários",History
http://stream.bplaymove.com:8080/dhistory/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="History" tvg-name="History HD" tvg-logo="https://s26.postimg.org/p91re6i0p/thehistorychannelMEGAMAXIPTV.jpg" group-title="Documentários",History HD
http://stream.bplaymove.com:8080/dhistory/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="ID: Investigação Discovery" tvg-name="Investigação Discovery" tvg-logo="https://s26.postimg.org/9z1w7cwwp/investigaçãodiscoveryMEGAMAXIPTV.jpg" group-title="Documentários",Investigação Discovery


#EXTINF:-1 tvg-id="NatGeo Wild" tvg-name="NatGeo Wild" tvg-logo="https://s26.postimg.org/bs4sv57nd/natgeowildMEGAMAXIPTV.jpg" group-title="Documentários",NatGeo Wild


#EXTINF:-1 tvg-id="NatGeo" tvg-name="National Geographic" tvg-logo="https://s26.postimg.org/ti6hg6sy1/nationalgeographicMEGAMAXIPTV.jpg" group-title="Documentários",National Geographic
http://stream.bplaymove.com:80/dnatgeo/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="NatGeo" tvg-name="National Geographic HD" tvg-logo="https://s26.postimg.org/ti6hg6sy1/nationalgeographicMEGAMAXIPTV.jpg" group-title="Documentários",National Geographic HD
http://stream.bplaymove.com:80/dnatgeohd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Documentários",

################## Filmes e Séries ##################

#EXTINF:-1 tvg-id="AMC" tvg-name="AMC" tvg-logo="https://s26.postimg.org/749dx8fa1/amc_MEGAMAXIPTV.jpg" group-title="Filmes e Séries",AMC
http://stream.bplaymove.com:80/famc/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="AMC" tvg-name="AMC HD" tvg-logo="https://s26.postimg.org/749dx8fa1/amc_MEGAMAXIPTV.jpg" group-title="Filmes e Séries",AMC HD
http://stream.bplaymove.com:80/famchd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="AXN" tvg-name="AXN" tvg-logo="https://s26.postimg.org/rp3ks17t5/axnMEGAMAXIPTV.jpg" group-title="Filmes e Séries",AXN
http://stream.bplaymove.com:80/faxn/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="AXN" tvg-name="AXN HD" tvg-logo="https://s26.postimg.org/rp3ks17t5/axnMEGAMAXIPTV.jpg" group-title="Filmes e Séries",AXN HD
http://stream.bplaymove.com:80/faxnhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Cinemax" tvg-name="Cinemax" tvg-logo="https://s26.postimg.org/vy8aua3dl/cinemaxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Cinemax
http://stream.bplaymove.com:80/fcinemax/video.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Cinemax" tvg-name="Cinemax HD" tvg-logo="https://s26.postimg.org/vy8aua3dl/cinemaxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Cinemax HD
http://stream.bplaymove.com:80/fcinemaxhd/video.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Fox" tvg-name="Fox" tvg-logo="https://s26.postimg.org/srdras8op/foxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Fox
http://stream.bplaymove.com:80/ffox/video.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Fox" tvg-name="Fox HD" tvg-logo="https://s26.postimg.org/srdras8op/foxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Fox HD
http://stream.bplaymove.com:80/ffoxhd/video.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Fox" tvg-name="Fox (Alternativo)" tvg-logo="https://s26.postimg.org/srdras8op/foxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Fox (Alternativo)


#EXTINF:-1 tvg-id="Fox Premium 1" tvg-name="Fox Premium 1" tvg-logo="https://s26.postimg.org/s1uyyffux/foxpremium1MEGAMAXIPTV.jpg" group-title="Filmes e Séries",Fox Premium 1


#EXTINF:-1 tvg-id="Fox Premium 2" tvg-name="Fox Premium 2" tvg-logo="https://s26.postimg.org/p7rtkzleh/foxpremium2MEGAMAXIPTV.jpg" group-title="Filmes e Séries",Fox Premium 2


#EXTINF:-1 tvg-id="FX" tvg-name="FX" tvg-logo="https://s26.postimg.org/z52ue28ft/fxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",FX
http://stream.bplaymove.com:80/ffx/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="FX" tvg-name="FX HD" tvg-logo="https://s26.postimg.org/z52ue28ft/fxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",FX HD
http://stream.bplaymove.com:80/ffxhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO [Brazil]" tvg-name="HBO" tvg-logo="https://s26.postimg.org/7hq502pux/hboMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO
http://stream.bplaymove.com:45127/fhbo/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO [Brazil]" tvg-name="HBO HD" tvg-logo="https://s26.postimg.org/7hq502pux/hboMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO HD
http://stream.bplaymove.com:45127/fhbohd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO 2 [Brazil]" tvg-name="HBO 2" tvg-logo="https://s26.postimg.org/lo5vvadkp/hbo2MRGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO 2
http://stream.bplaymove.com:45127/fhbo2/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO 2 [Brazil]" tvg-name="HBO 2 HD" tvg-logo="https://s26.postimg.org/lo5vvadkp/hbo2MRGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO 2 HD
http://stream.bplaymove.com:45127/fhbo2hd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO Family [Brazil]" tvg-name="HBO Family" tvg-logo="https://s26.postimg.org/uj6q5ta2x/hbofamilyMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Family


#EXTINF:-1 tvg-id="HBO Family [Brazil]" tvg-name="HBO Family HD" tvg-logo="https://s26.postimg.org/uj6q5ta2x/hbofamilyMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Family HD
http://stream.bplaymove.com:45127/fhbofamilyhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO Signature [Brazil]" tvg-name="HBO Signature" tvg-logo="https://s26.postimg.org/qmte9uc8p/hbosignatureMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Signature
http://stream.bplaymove.com:45127/fhbosignature/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO Signature [Brazil]" tvg-name="HBO Signature HD" tvg-logo="https://s26.postimg.org/qmte9uc8p/hbosignatureMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Signature HD
http://stream.bplaymove.com:45127/fhbosignaturehd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO Plus [Brazil]" tvg-name="HBO Plus" tvg-logo="https://s26.postimg.org/50edstle1/hboplusMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Plus


#EXTINF:-1 tvg-id="HBO Plus [Brazil]" tvg-name="HBO Plus HD" tvg-logo="https://s26.postimg.org/50edstle1/hboplusMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Plus HD
http://stream.bplaymove.com:45127/fhboplushd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="HBO Plus *e [Brazil]" tvg-name="HBO Plus *e" tvg-logo="https://s26.postimg.org/jwcx0e9nd/hbopluseMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Plus *e
http://stream.bplaymove.com:45127/fhbopluse/index.m3u8?MEGAMAXIPTV

EXTINF:-1 tvg-id="HBO Plus *e [Brazil]" tvg-name="HBO Plus *e HD" tvg-logo="https://s26.postimg.org/jwcx0e9nd/hbopluseMEGAMAXIPTV.jpg" group-title="Filmes e Séries",HBO Plus *e HD


#EXTINF:-1 tvg-id="Max [Brazil]" tvg-name="Max" tvg-logo="https://s26.postimg.org/eyzceqa2x/maxMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Max
http://stream.bplaymove.com:80/fmaxprimee/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Max Prime [Brazil]" tvg-name="Max Prime" tvg-logo="https://s26.postimg.org/5efprusgp/maxprimeMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Max Prime
http://stream.bplaymove.com:80/fmaxprime/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Max Prime [Brazil]" tvg-name="Max Prime HD" tvg-logo="https://s26.postimg.org/5efprusgp/maxprimeMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Max Prime HD
http://stream.bplaymove.com:80/fmaxprimehd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Max Prime [Brazil]" tvg-name="Max Prime *e" tvg-logo="https://s26.postimg.org/5efprusgp/maxprimeMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Max Prime *e
http://stream.bplaymove.com:80/fmaxprimee/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Max UP" tvg-name="Max UP" tvg-logo="https://s26.postimg.org/plt5k5xnt/maxupMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Max UP
http://stream.bplaymove.com:8080/fmaxup/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Max UP" tvg-name="Max UP HD" tvg-logo="https://s26.postimg.org/plt5k5xnt/maxupMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Max UP HD
http://stream.bplaymove.com:8080/fmaxuphd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Megapix" tvg-name="Megapix" tvg-logo="https://s26.postimg.org/u7p9siymh/megapixMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Megapix
http://stream.bplaymove.com:8080/fmegapix/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Megapix" tvg-name="Megapix HD" tvg-logo="https://s26.postimg.org/u7p9siymh/megapixMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Megapix HD
http://stream.bplaymove.com:8080/fmegapixhd/index.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="Paramount Channel" tvg-name="Paramount Channel" tvg-logo="https://s26.postimg.org/pykjqf0ix/paramountchannelMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Paramount Channel
http://stream.bplaymove.com:80/fparamount/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Sony" tvg-name="Sony" tvg-logo="https://s26.postimg.org/aokojezxl/canalsonyMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Sony
http://stream.bplaymove.com:80/fsony/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Space" tvg-name="Space" tvg-logo="https://s26.postimg.org/3ze538tzd/spaceMEGAMXIPTV.jpg" group-title="Filmes e Séries",Space
http://stream.bplaymove.com:80/fspace/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Space" tvg-name="Space HD" tvg-logo="https://s26.postimg.org/3ze538tzd/spaceMEGAMXIPTV.jpg" group-title="Filmes e Séries",Space HD
http://stream.bplaymove.com:80/fspacehd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Studio Universal" tvg-name="Studio Universal" tvg-logo="https://s26.postimg.org/4c5j9fzeh/studiouniversalMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Studio Universal
http://stream.bplaymove.com:80/fstudiouniversal/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Studio Universal" tvg-name="Studio Universal HD" tvg-logo="https://s26.postimg.org/4c5j9fzeh/studiouniversalMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Studio Universal HD
http://stream.bplaymove.com:80/fstudiouniversalhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Syfy" tvg-name="Syfy" tvg-logo="https://s26.postimg.org/vzi8nk00p/syfyMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Syfy
http://stream.bplaymove.com:80/fsyfy/index.m3u8?

#EXTINF:-1 tvg-id="Syfy" tvg-name="Syfy HD" tvg-logo="https://s26.postimg.org/vzi8nk00p/syfyMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Syfy HD
http://stream.bplaymove.com:80/fsyfyhd/index.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="TBS" tvg-name="TBS" tvg-logo="https://s26.postimg.org/5r73y6vcp/tbsMEGAMAXIPTV.jpg" group-title="Filmes e Séries",TBS
http://stream.bplaymove.com:80/ftbs/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TBS" tvg-name="TBS HD" tvg-logo="https://s26.postimg.org/5r73y6vcp/tbsMEGAMAXIPTV.jpg" group-title="Filmes e Séries",TBS HD
http://stream.bplaymove.com:80/ftbshd/index.m3u8?MEGAMAXIPTV 

#EXTINF:-1 tvg-id="TCM" tvg-name="TCM" tvg-logo="" group-title="Filmes e Séries",TCM
http://stream.bplaymove.com:80/ftcm/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Action [Brazil]" tvg-name="Telecine Action" tvg-logo="https://s26.postimg.org/i5tvyj2ah/telecineactionMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Action
http://stream.bplaymove.com:45127/ftcaction/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Action [Brazil]" tvg-name="Telecine Action HD" tvg-logo="https://s26.postimg.org/i5tvyj2ah/telecineactionMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Action HD
http://stream.bplaymove.com:45127/ftctouchhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Cult [Brazil]" tvg-name="Telecine Cult" tvg-logo="https://s26.postimg.org/hgb3m69gp/telecinecultMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Cult
http://stream.bplaymove.com:45127/ftccult/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Cult [Brazil]" tvg-name="Telecine Cult HD" tvg-logo="https://s26.postimg.org/hgb3m69gp/telecinecultMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Cult HD
http://stream.bplaymove.com:45127/ftcculthd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Fun [Brazil]" tvg-name="Telecine Fun" tvg-logo="https://s26.postimg.org/o6rkvm4c9/telecinefunMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Fun
http://stream.bplaymove.com:45127/ftcfun/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Fun [Brazil]" tvg-name="Telecine Fun HD" tvg-logo="https://s26.postimg.org/o6rkvm4c9/telecinefunMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Fun HD
http://stream.bplaymove.com:45127/ftcfunhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Pipoca [Brazil]" tvg-name="Telecine Pipoca" tvg-logo="https://s26.postimg.org/ti6hgby4p/telecinepipocaMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Pipoca
http://stream.bplaymove.com:45127/ftcpipoca/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Pipoca [Brazil]" tvg-name="Telecine Pipoca HD" tvg-logo="https://s26.postimg.org/ti6hgby4p/telecinepipocaMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Pipoca HD
http://stream.bplaymove.com:45127/ftcpipocahd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Premium [Brazil]" tvg-name="?MEGAMAXIPTV" tvg-logo="https://s26.postimg.org/3ze53bmah/telecinepremiumMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Premium
http://stream.bplaymove.com:45127/ftcpremium/index.m3u8

#EXTINF:-1 tvg-id="Telecine Premium [Brazil]" tvg-name="" tvg-logo="https://s26.postimg.org/3ze53bmah/telecinepremiumMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Premium HD
http://stream.bplaymove.com:45127/ftcpremiumhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Touch [Brazil]" tvg-name="Telecine Touch" tvg-logo="https://s26.postimg.org/r0uq92tnt/telecinetouchMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Touch
http://stream.bplaymove.com:45127/ftctouch/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Telecine Touch [Brazil]" tvg-name="Telecine Touch HD" tvg-logo="https://s26.postimg.org/r0uq92tnt/telecinetouchMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Telecine Touch HD
http://stream.bplaymove.com:45127/ftctouchhd/index.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="TNT" tvg-name="TNT" tvg-logo="https://s26.postimg.org/8y1nhvkyh/tntMEGAMAXIPTV.jpg" group-title="Filmes e Séries",TNT
http://stream.bplaymove.com:80/ftnt/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TNT" tvg-name="TNT HD" tvg-logo="https://s26.postimg.org/8y1nhvkyh/tntMEGAMAXIPTV.jpg" group-title="Filmes e Séries",TNT HD
http://stream.bplaymove.com:80/ftnthd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TNT Series" tvg-name="TNT Series HD" tvg-logo="https://s26.postimg.org/7j02t5rl5/tntseriesMEGAMAXIPTV.jpg" group-title="Filmes e Séries",TNT Series HD
http://stream.bplaymove.com:80/ftntserieshd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Universal Channel" tvg-name="Universal" tvg-logo="https://s26.postimg.org/ivcoazq09/universalchannelMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Universal Channel

#EXTINF:-1 tvg-id="Universal Channel" tvg-name="Universal HD" tvg-logo="https://s26.postimg.org/ivcoazq09/universalchannelMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Universal Channel HD
http://stream.bplaymove.com:8080/funiversalhd/index.m3u8?MEGAMAXIPTV


#EXTINF:-1 tvg-id="Warner Channel" tvg-name="Warner Channel" tvg-logo="https://s26.postimg.org/ivcob0d5l/warnerchannelMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Warner Channel
http://stream.bplaymove.com:80/fwarner/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Warner Channel" tvg-name="Warner Channel HD" tvg-logo="https://s26.postimg.org/ivcob0d5l/warnerchannelMEGAMAXIPTV.jpg" group-title="Filmes e Séries",Warner Channel HD
http://stream.bplaymove.com:80/fwarnerhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Filmes e Séries",

################## Infantil ##################

#EXTINF:-1 tvg-id="BabyTV" tvg-name="Baby TV" tvg-logo="https://s26.postimg.org/expeljl6h/babytvMEGAMAXIPTV.jpg" group-title="Infantil",Baby TV
http://stream.bplaymove.com:80/ibabytv/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Boomerang" tvg-name="Boomerang" tvg-logo="https://s26.postimg.org/oi918g5dl/boomerangMEGAMAXIPTV.jpg" group-title="Infantil",Boomerang
http://stream.bplaymove.com:80/iboomerang/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Boomerang" tvg-name="Boomerang HD" tvg-logo="https://s26.postimg.org/oi918g5dl/boomerangMEGAMAXIPTV.jpg" group-title="Infantil",Boomerang HD

#EXTINF:-1 tvg-id="Cartoon Network" tvg-name="Cartoon Network" tvg-logo="https://s26.postimg.org/qmte9k1vd/cartoonnetworkMEGAMAXIPTV.jpg" group-title="Infantil",Cartoon Network
http://stream.bplaymove.com:80/icartoonnetwork/index.m3u8?MEGAMAXIPTV 

#EXTINF:-1 tvg-id="Cartoon Retrô" tvg-name="Cartoon Retrô" tvg-logo="https://s26.postimg.org/bquv1yy6h/cartoonretroMEGAMAXIPTV.jpg" group-title="Infantil",Cartoon Retrô

#EXTINF:-1 tvg-id="Discovery Kids" tvg-name="Discovery Kids" tvg-logo="https://s26.postimg.org/foilalwqx/discoverykids_MEGAMAXIPTV.jpg" group-title="Infantil",Discovery Kids
http://stream.bplaymove.com:80/idiscoverykids/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Disney Channel" tvg-name="Disney Channel" tvg-logo="https://s26.postimg.org/p7rtkx0t5/disneychannelMEGAMAXIPTV.jpg" group-title="Infantil",Disney Channel
http://stream.bplaymove.com:80/idisney/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Disney Junior" tvg-name="Disney Junior" tvg-logo="https://s26.postimg.org/mdoo7gqx5/disneyjuniorMEGAMAXIPTV.jpg" group-title="Infantil",Disney Junior
http://stream.bplaymove.com:80/idisneyjr/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Disney XD" tvg-name="Disney XD" tvg-logo="https://s26.postimg.org/nfyuq1cbd/disneyxdMEGAMAXIPTV.jpg" group-title="Infantil",Disney XD
http://stream.bplaymove.com:80/idisneyxd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Gloob [Brazil]" tvg-name="Gloob" tvg-logo="https://s26.postimg.org/6ffyhi4gp/gloobMEGAMAXIPTV.jpg" group-title="Infantil",Gloob
http://stream.bplaymove.com:80/igloob/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Gloob [Brazil]" tvg-name="Gloob HD" tvg-logo="https://s26.postimg.org/6ffyhi4gp/gloobMEGAMAXIPTV.jpg" group-title="Infantil",Gloob HD
http://stream.bplaymove.com:80/igloobhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="NatGeo Kids" tvg-name="NatGeo Kids" tvg-logo="https://s26.postimg.org/d76u3czzt/natgeokids_MEGAMAXIPTV.jpg" group-title="Infantil",NatGeo Kids
http://stream.bplaymove.com:80/inatgeokids/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="NatGeo Kids" tvg-name="NatGeo Kids HD" tvg-logo="https://s26.postimg.org/d76u3czzt/natgeokids_MEGAMAXIPTV.jpg" group-title="Infantil",NatGeo Kids HD
http://stream.bplaymove.com:80/inatgeokidsHD/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Nick" tvg-name="Nick" tvg-logo="https://26.postimg.org/ssnp3u7u1/nickelodeonMEGAMAXIPTV.jpg" group-title="Infantil",Nick
shttp://stream.bplaymove.com:80/inick/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Nick" tvg-name="Nick HD" tvg-logo="https://26.postimg.org/ssnp3u7u1/nickelodeonMEGAMAXIPTV.jpg" group-title="Infantil",Nick HD
http://stream.bplaymove.com:80/inickelodeonhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Nick Jr." tvg-name="Nick Jr." tvg-logo="https://s26.postimg.org/bs4sv62ih/nickjrMEGAMAXIPTV.jpg" group-title="Infantil",Nick Jr.
http://stream.bplaymove.com:8080/inickjr/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Nick Jr." tvg-name="Nick Jr. HD" tvg-logo="https://s26.postimg.org/bs4sv62ih/nickjrMEGAMAXIPTV.jpg" group-title="Infantil",Nick Jr. HD
http://stream.bplaymove.com:8080/inickjrhd/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="TV Rá-Tim-Bum" tvg-name="TV Rá-Tim-Bum" tvg-logo="https://s26.postimg.org/9nkfuab89/tvrá-tim-bumMEGAMAXIPTV.jpg" group-title="Infantil",TV Rá-Tim-Bum


#EXTINF:-1 tvg-id="Tooncast" tvg-name="Tooncast" tvg-logo="https://s26.postimg.org/m277uksft/tooncastMEGAMAXIPTV.jpg" group-title="Infantil",Tooncast
http://stream.bplaymove.com:80/itooncast/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Zoomoo" tvg-name="Zoomoo" tvg-logo="https://s26.postimg.org/5efps5i9l/zoomooMEGAMAXIPTV.jpg" group-title="Infantil",Zoomoo

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Infantil",


################## Notícias ##################


#EXTINF:-1 tvg-id="BandNews" tvg-name="Band News" tvg-logo="https://s26.postimg.org/5px64utjt/bandnewsMEGAMAXIPTV.jpg" group-title="Notícias",Band News
http://stream.bplaymove.com:80/nbandnews/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Bloomberg" tvg-name="Bloomberg" tvg-logo="https://s26.postimg.org/9z1w701y1/bloomergMEGAMAXIPTV.jpg" group-title="Notícias",Bloomberg

#EXTINF:-1 tvg-id="CNNi" tvg-name="CNN Internacional" tvg-logo="https://s26.postimg.org/j6u4nsgqx/cnnMEGAMAXIPTV.jpg" group-title="Notícias",CNN Internacional

#EXTINF:-1 tvg-id="GloboNews" tvg-name="Globo News" tvg-logo="https://s26.postimg.org/pxalxdoix/globonewsMEGAMAXIPTV.jpg" group-title="Notícias",Globo News


#EXTINF:-1 tvg-id="Record News" tvg-name="Record News" tvg-logo="https://s26.postimg.org/qo3c2svxl/recordnewsMEGAMAXIPTV.jpg" group-title="Notícias",Record News
http://stream.bplaymove.com:8080/nrecordnews/index.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="Record News" tvg-name="Record News HD" tvg-logo="https://s26.postimg.org/qo3c2svxl/recordnewsMEGAMAXIPTV.jpg" group-title="Notícias",Record News HD
http://stmv4.srvstm.com/recordnewses/recordnewses/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="" group-title="Notícias",

################## Câmeras Ao Vivo ##################

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Aeroporto de Congonhas SP
https://d1h84if288zv9w.cloudfront.net/7dias/0be3_135.stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Animais África do Sul
http://stream.africam.com:1935/tm/tm.stream_360p/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Estúdio 100.5 FM RJ
http://c2901-slbps-sambavideos.akamaized.net/livet/FMoDiaVideo_14d480346fa04ebcb4f03173f2dae707/ngrp:livestream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Estúdio 102 FM Itajaí - SC
http://streaming2.aquisolucoes.com.br:1935/radio102video/aovivo/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Estúdio CBN 90.5 FM SP
http://media.sgr.globo.com:1935/VideoCBN/cbnsp1.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Estúdio Rádio Jornal Recife
http://evpp.mm.uol.com.br:1935/ne10/ne10-radiojornal-recife-video-web.sdp/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Marginal Pinheiros Trânsito
https://d1h84if288zv9w.cloudfront.net/7dias/0be3_69.stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",Santuário Aparecida
http://caikrondatacenter.com.br:1935/santuario_aparecida/canal5.stream/playlist.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-logo="https://s26.postimg.org/jaotjga8p/camerasaovivoMEGAMAXIPTV.jpg" group-title="Câmeras Ao Vivo",




################## Rádios ##################

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Anos 70
http://192.99.150.31:8241/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Anos 80
https://stream.vagalume.fm/hls/1507663468882713/aac.m3u8?MEGAMAXIPTV
 
#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Anos 90
https://stream.vagalume.fm/hls/1478022035646498/aac.m3u8?MEGAMAXIPTV
 
#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Anos 2000
https://stream.vagalume.fm/hls/147396460892793/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Axé
http://192.99.150.31:8305/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Bossa Nova
http://6d9a.webradio.upx.net.br:9974/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Eletrônica
https://stream.vagalume.fm/hls/146411300413492499/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Eletrônica 2
http://192.99.150.31:9095/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Festa
http://6d9a.webradio.upx.net.br:9070/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Folk
https://stream.vagalume.fm/hls/1470155259220668/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Forró
https://stream.vagalume.fm/hls/147015501223418/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Funk
https://stream.vagalume.fm/hls/1470154922349875/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Funk 2
http://192.99.150.31:9089/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Gospel
https://stream.vagalume.fm/hls/14658544132100453351/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Gospel 2
http://192.99.150.31:9101/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Gospel Internacional
https://stream.vagalume.fm/hls/1470245767122628/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Hip-Hop
https://stream.vagalume.fm/hls/1470155107337444/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Hip-Hop 2
http://192.99.150.31:8721/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Jazz
http://192.99.150.31:8042/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",J-Pop
https://stream.vagalume.fm/hls/1475776167864381/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Kids
http://192.99.150.31:9431/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Latina
https://stream.vagalume.fm/hls/1478199774246251/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Lounge
http://6d9a.webradio.upx.net.br:9064/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Metal
http://6d9a.webradio.upx.net.br:9898/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",MPB
https://stream.vagalume.fm/hls/1470154983100588/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",MPB 2
http://192.99.150.31:8775/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Música Clássica
http://6d9a.webradio.upx.net.br:9970/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",New Rock
http://6d9a.webradio.upx.net.br:8028/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pagode
https://stream.vagalume.fm/hls/147015499779090/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pagode 2
http://6d9a.webradio.upx.net.br:9061/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pop
https://stream.vagalume.fm/hls/14660050141766497579/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pop Hits
http://6d9a.webradio.upx.net.br:9046/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pop Rock
https://stream.vagalume.fm/hls/1506975770142563/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pop Rock 2
http://6d9a.webradio.upx.net.br:8016/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pop Nacional
https://stream.vagalume.fm/hls/1470169276225492/aac.m3u8?MEGAMAXIPTV
 
#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Pop Rock Nacional
https://stream.vagalume.fm/hls/14671417541091220806/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Rap Nacional
http://192.99.150.31:8377/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Reggae
https://stream.vagalume.fm/hls/1465840912218182609/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Reggae 2
http://192.99.150.31:8715/stream?MEGAMAXIPTV
 

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Rock 
https://stream.vagalume.fm/hls/14647977281792658143/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Rock 2
https://stream.vagalume.fm/hls/14647977281792658143/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Rock Clássico
http://192.99.150.31:9125/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Românticas
https://stream.vagalume.fm/hls/1470155219129532/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Românticas 2
http://192.99.150.31:8962/stream?MEGAMAXIPTV
 
#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Rock
https://stream.vagalume.fm/hls/1464201608479108132/aac.m3u8?MEGAMAXIPTV
 
#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Sertanejo
https://stream.vagalume.fm/hls/14619606471054026608/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Sertanejo 2
http://192.99.150.31:9485/stream?MEGAMAXIPTV
 
#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Sertanejo Romântico
https://stream.vagalume.fm/hls/1499715905423293/aac.m3u8?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Sertanejo Universitário
http://192.99.150.31:9191/stream?MEGAMAXIPTV

#EXTINF:-1 tvg-logo="https://s26.postimg.org/70ndlup15/radios_MEGAMAXIPTV.png" group-title="Rádios",Trilhas Sonoras
http://6d9a.webradio.upx.net.br:9896/stream?MEGAMAXIPTV

################################################################################ "Desenhos e Séries 24hrs"
 
 
################################## Série: YOUNG SHELDON ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E01
http://www.blogger.com/video-play.mp4?contentId=9828da2968a97576&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E02
http://www.blogger.com/video-play.mp4?contentId=80a52844051fddc3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E03
http://www.blogger.com/video-play.mp4?contentId=567656a34fa4eb40&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E04
http://www.blogger.com/video-play.mp4?contentId=4951f1c1ca3bacf9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E05
http://www.blogger.com/video-play.mp4?contentId=16b40279940f6f05&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E06
http://www.blogger.com/video-play.mp4?contentId=787a27474a32b294&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E07
http://www.blogger.com/video-play.mp4?contentId=7e0bc6f811ed9d20&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E08
http://www.blogger.com/video-play.mp4?contentId=a1939764527720a1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E09
http://www.blogger.com/video-play.mp4?contentId=386f35221f3a2dda&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E10
http://www.blogger.com/video-play.mp4?contentId=94e5ceb6649bf8f8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E11
http://www.blogger.com/video-play.mp4?contentId=9f43096e9e6a7476&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E12
http://www.blogger.com/video-play.mp4?contentId=87e34f3fb8d2df45&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/f0qbssy21/youngsheldonse1_MEGAMAXIPTV.jpg" group-title="Série: YOUNG SHELDON",YOUNG SHELDON T01|E13
http://www.blogger.com/video-play.mp4?contentId=05c17f07781017cd&MEGAMAXIPTV

################################## Série: LA CASA DE PAPEL ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E01
https://www.blogger.com/video-play.mp4?contentId=493119be16ec0c3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E02
https://www.blogger.com/video-play.mp4?contentId=af637567073d026f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E03
https://www.blogger.com/video-play.mp4?contentId=db60e824145b73ba&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E04
https://www.blogger.com/video-play.mp4?contentId=91bb3402a67a3527&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E05
https://www.blogger.com/video-play.mp4?contentId=62f71f3b893a865b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E06
https://www.blogger.com/video-play.mp4?contentId=c0835429ccaa0d6a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E07
https://www.blogger.com/video-play.mp4?contentId=e217d5ac1d337bac&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E08
https://www.blogger.com/video-play.mp4?contentId=410dd9c548901fc2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E09
https://www.blogger.com/video-play.mp4?contentId=2af103b0f5e2e9b4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E10
https://www.blogger.com/video-play.mp4?contentId=1f98cbac839c523e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E11
https://www.blogger.com/video-play.mp4?contentId=268b4ed568d799a2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E12
https://www.blogger.com/video-play.mp4?contentId=e0293667701129f1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/708tcl3y1/lacasadepapelse1_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL T01|E13
https://www.blogger.com/video-play.mp4?contentId=a1963c2a4f60812b&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/thdl74ngp/lacasadepapel_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL(LEG) T02|E01
http://www.blogger.com/video-play.mp4?contentId=94cdb161615ac3f9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/thdl74ngp/lacasadepapel_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL(LEG) T02|E02
http://www.blogger.com/video-play.mp4?contentId=cb5a3dd9d79fdbb7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/thdl74ngp/lacasadepapel_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL(LEG) T02|E03
http://www.blogger.com/video-play.mp4?contentId=99057d3992b7976a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/thdl74ngp/lacasadepapel_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL(LEG) T02|E04
http://www.blogger.com/video-play.mp4?contentId=44a2e50881b7fe83&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/thdl74ngp/lacasadepapel_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL(LEG) T02|E05
http://www.blogger.com/video-play.mp4?contentId=283c0041d9afba5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/thdl74ngp/lacasadepapel_MEGAMAXIPTV.jpg" group-title="Série: LA CASA DE PAPEL",LA CASA DE PAPEL(LEG) T02|E06
http://www.blogger.com/video-play.mp4?contentId=2a8cf4b2e65b0ab0&MEGAMAXIPTV


################################## Série: ALTERED CARBON ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E01
http://www.blogger.com/video-play.mp4?contentId=e5f295dad5b4d796&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E02
http://www.blogger.com/video-play.mp4?contentId=26a9dba5b81a1bfa&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E03
http://www.blogger.com/video-play.mp4?contentId=4fc12a91448b0dcc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E04
http://www.blogger.com/video-play.mp4?contentId=5fefaa18e589ecff&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E05
http://www.blogger.com/video-play.mp4?contentId=a0bc8e7f9dd02c67&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E06
http://www.blogger.com/video-play.mp4?contentId=a6ff637ba47c718b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E07
http://www.blogger.com/video-play.mp4?contentId=a82b729ab1c2f3e4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E08
http://www.blogger.com/video-play.mp4?contentId=8e2eea1102a20e93&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E09
http://www.blogger.com/video-play.mp4?contentId=d9754ba6e1c4bcf9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série: ALTERED CARBON",ALTERED CARBON T01|E10
http://www.blogger.com/video-play.mp4?contentId=7e3ede55644a19aa&MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E01
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E02
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E03
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E04
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E05
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E06
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E07
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E08
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E09
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/58fuho55l/alteredcarbon_MEGAMAXIPTV.jpg" group-title="Série HD: ALTERED CARBON",ALTERED CARBON (HD) T01|E10
http://cdn1.ntcdn.us/content/httpdelivery/ac/01dub/10-ALTO.mp4?MEGAMAXIPTV


################################## Série: NARCOS ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/01-BAIXO.mp4
?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/02-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/03-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/04-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/05-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/06-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/07-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/08-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/09-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T01|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/01dub/10-BAIXO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/01-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/02-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/03-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/04-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/05-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/06-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/07-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/08-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/09-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS(LEG) T02|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/narcos/02leg/10-BAIXO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E01
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/01-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E02
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/02-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E03
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/03-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E04
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/04-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E05
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/05-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E06
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/06-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E07
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/07-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E08
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/08-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E09
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/09-BAIXO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/pfta9zacp/narcosmegamaxiptv.jpg" group-title="Série: NARCOS",NARCOS T03|E10
http://cdn1.ntcdn.us/content/httpdelivery/narcos/03dub/10-BAIXO.mp4?MEGAMAXIPTV


################################## Série: THE FLASH ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E01
https://www.blogger.com/video-play.mp4?contentId=9c0bb457850984ff&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E02
https://www.blogger.com/video-play.mp4?contentId=f77c796c53e1f829&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E03
https://www.blogger.com/video-play.mp4?contentId=d1069902508fd4dc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E04
https://www.blogger.com/video-play.mp4?contentId=ad40a942491acdee&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E05
https://www.blogger.com/video-play.mp4?contentId=a63340306ebb126&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E06
https://www.blogger.com/video-play.mp4?contentId=fecbc188edc21ba1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E07
https://www.blogger.com/video-play.mp4?contentId=6ad11fef30e7382a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E08
https://www.blogger.com/video-play.mp4?contentId=466da77b707b0829&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E09
https://www.blogger.com/video-play.mp4?contentId=e6dac14f46c5c2fc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E10
https://www.blogger.com/video-play.mp4?contentId=94b80bb85a2e8701&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E11
https://www.blogger.com/video-play.mp4?contentId=673d0ba342eb35a0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E12
https://www.blogger.com/video-play.mp4?contentId=d0ecdb617304d893&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E13
https://www.blogger.com/video-play.mp4?contentId=622a117a27766402&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E14
https://www.blogger.com/video-play.mp4?contentId=8ac3e5f7cb2c1fde&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E15
https://www.blogger.com/video-play.mp4?contentId=881acd462762b5d8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E16
https://www.blogger.com/video-play.mp4?contentId=8692b7ba5ffa374a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E17
https://www.blogger.com/video-play.mp4?contentId=5aad36c33e885796&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E18
https://www.blogger.com/video-play.mp4?contentId=ff948913364e0b15&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E19
https://www.blogger.com/video-play.mp4?contentId=fc6248d0e0586848&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E20
https://www.blogger.com/video-play.mp4?contentId=d459f60ea737870f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E21
https://www.blogger.com/video-play.mp4?contentId=c705bb58caa3e26b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E22
https://www.blogger.com/video-play.mp4?contentId=328ef7f96b4987b6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xksln0rsp/theflashse1_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T01|E23
https://www.blogger.com/video-play.mp4?contentId=6cc01b97ff3f9f2d&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E01
https://www.blogger.com/video-play.mp4?contentId=49bad75da79a111c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E02
https://www.blogger.com/video-play.mp4?contentId=39be00324378041c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E03
https://www.blogger.com/video-play.mp4?contentId=378b9bbe9b92a9cd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E04
https://www.blogger.com/video-play.mp4?contentId=79e341122ec9024b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E05
https://www.blogger.com/video-play.mp4?contentId=5e12184b0d6e1450&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E06
https://www.blogger.com/video-play.mp4?contentId=e685ca183ed6f990&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E07
https://www.blogger.com/video-play.mp4?contentId=80052f8bc61daf0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E08
https://www.blogger.com/video-play.mp4?contentId=856bd175b7c518c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E09
https://www.blogger.com/video-play.mp4?contentId=af0eefd58bfebb13&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E10
https://www.blogger.com/video-play.mp4?contentId=243044cb469f980a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E11
https://www.blogger.com/video-play.mp4?contentId=c2c0a9d0c0049ddc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E12
https://www.blogger.com/video-play.mp4?contentId=1c5ad71796fea061&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E13
https://www.blogger.com/video-play.mp4?contentId=701ce47a7cfb86e6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E14
https://www.blogger.com/video-play.mp4?contentId=74a646579df897cb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E15
https://www.blogger.com/video-play.mp4?contentId=67aeb37031baee6c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E16
https://www.blogger.com/video-play.mp4?contentId=954da779d6a02480&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E17
https://www.blogger.com/video-play.mp4?contentId=6c132064fb8c4e1d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E18
https://www.blogger.com/video-play.mp4?contentId=3e574a05845de8a4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E19
https://www.blogger.com/video-play.mp4?contentId=d4072305d85de8e0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E20
https://www.blogger.com/video-play.mp4?contentId=93dc37f47769f00c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E21
https://www.blogger.com/video-play.mp4?contentId=3d123c73583f9091&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E22
https://www.blogger.com/video-play.mp4?contentId=4cb793118bb1f8f7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/8erng7ldl/theflashse2_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T02|E23
https://www.blogger.com/video-play.mp4?contentId=56b7bbcba557bcb2&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E01
https://www.blogger.com/video-play.mp4?contentId=a4bde66b86afb201&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E02
https://www.blogger.com/video-play.mp4?contentId=8ae8623a8dd581f9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E03
https://www.blogger.com/video-play.mp4?contentId=eb76ce325ff1f9ed&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E04
https://www.blogger.com/video-play.mp4?contentId=c475f62b1fea59aa&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E05
https://www.blogger.com/video-play.mp4?contentId=9cc7b359777dd36a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E06
https://www.blogger.com/video-play.mp4?contentId=fce60168cddec0ef&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E07
https://www.blogger.com/video-play.mp4?contentId=ac309898a13467a5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E08
https://www.blogger.com/video-play.mp4?contentId=3fb33834e758ce19&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E09
https://www.blogger.com/video-play.mp4?contentId=e2f01e6c850ea7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E10
https://www.blogger.com/video-play.mp4?contentId=5c184fe0d83b42e6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E11
https://www.blogger.com/video-play.mp4?contentId=a6e30f447d06ed59&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E12
https://www.blogger.com/video-play.mp4?contentId=cc78ccbdf897fe7c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E13
https://www.blogger.com/video-play.mp4?contentId=57c5bc890cbbf807&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E14
https://www.blogger.com/video-play.mp4?contentId=f2fc9c0bc728b38c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E15
https://www.blogger.com/video-play.mp4?contentId=a87ca6a20890ec8e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E16
https://www.blogger.com/video-play.mp4?contentId=d825040048187bcd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E17
https://www.blogger.com/video-play.mp4?contentId=a3587156df3be996&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E18
https://www.blogger.com/video-play.mp4?contentId=4425c6a11ffeb6de&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E19
https://www.blogger.com/video-play.mp4?contentId=1c5aa85c0dd30fcf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E20
https://www.blogger.com/video-play.mp4?contentId=7a3d451d6c4f8e84&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E21
https://www.blogger.com/video-play.mp4?contentId=f39aa02cc303d703&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E22
https://www.blogger.com/video-play.mp4?contentId=6fa4500f0ff3bdf4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/xxjzt72cp/theflashse3_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T03|E23
https://www.blogger.com/video-play.mp4?contentId=1479ac42507831a&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E01
https://www.blogger.com/video-play.mp4?contentId=cb8ef146177cc291&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E02
https://www.blogger.com/video-play.mp4?contentId=e3a09dfb057d7f46&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E03
https://www.blogger.com/video-play.mp4?contentId=f6e9ffc54e7de97f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E04
https://www.blogger.com/video-play.mp4?contentId=f9c77fdd656d61a8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E05
https://www.blogger.com/video-play.mp4?contentId=85abb90425728717&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E06
https://www.blogger.com/video-play.mp4?contentId=a9eb7663cebf28b4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E07
https://www.blogger.com/video-play.mp4?contentId=48e0c5583ceaebcf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E08
https://www.blogger.com/video-play.mp4?contentId=23151c0593d7c1c5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E09
https://www.blogger.com/video-play.mp4?contentId=d4d5395d649a920e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E10
https://www.blogger.com/video-play.mp4?contentId=a1d4eb1d02368ed9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b8ustnfu1/theflashse4_MEGAMAXIPTV.jpg" group-title="Série: THE FLASH",THE FLASH T04|E11
https://www.blogger.com/video-play.mp4?contentId=7716c2bad4e2cb42&MEGAMAXIPTV

################################## Série: THE WALKING DEAD ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/r01tzuvuh/thewalkingdeadse1_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T01|E01
http://www.blogger.com/video-play.mp4?contentId=1fb3264a56e3bee7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/r01tzuvuh/thewalkingdeadse1_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T01|E02
http://www.blogger.com/video-play.mp4?contentId=50137ed65043e664&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/r01tzuvuh/thewalkingdeadse1_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T01|E03
http://www.blogger.com/video-play.mp4?contentId=5471a18f51b9a4af&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/r01tzuvuh/thewalkingdeadse1_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T01|E04
http://www.blogger.com/video-play.mp4?contentId=b7a7ffb51d4017ef&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/r01tzuvuh/thewalkingdeadse1_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T01|E05
http://www.blogger.com/video-play.mp4?contentId=847fc06e0b3ff39a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/r01tzuvuh/thewalkingdeadse1_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T01|E06
http://www.blogger.com/video-play.mp4?contentId=d8e2f1407c4feb6e&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E01
http://www.blogger.com/video-play.mp4?contentId=9fa3ad114ec7ece7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E02
http://www.blogger.com/video-play.mp4?contentId=c8a3f04a2bee3f6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E03
http://www.blogger.com/video-play.mp4?contentId=622455327c6a0aab&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E04
http://www.blogger.com/video-play.mp4?contentId=668df46ac1a93ab1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E05
http://www.blogger.com/video-play.mp4?contentId=d05e8938452fb888&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E06
http://www.blogger.com/video-play.mp4?contentId=d3c6ade11d4c261d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E07
http://www.blogger.com/video-play.mp4?contentId=c6f77a93db211106&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E08
http://www.blogger.com/video-play.mp4?contentId=c3d93c4a44c7e9b1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E09
http://www.blogger.com/video-play.mp4?contentId=5f1cc3f6cbb1df00&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E10
http://www.blogger.com/video-play.mp4?contentId=d6a4a01e0653e5da&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E11
http://www.blogger.com/video-play.mp4?contentId=2e75845f4712600d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E12
http://www.blogger.com/video-play.mp4?contentId=66b9ad28396d1710&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/mpn64ubi1/thewalkingdeadse2_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T02|E13
http://www.blogger.com/video-play.mp4?contentId=1fbc0bb946d132c2&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E01
http://www.blogger.com/video-play.mp4?contentId=d87b32621890a282&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E02
http://www.blogger.com/video-play.mp4?contentId=9a55a6ec5033fee7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E03
http://www.blogger.com/video-play.mp4?contentId=cb008d0be0dc0fa6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E04
http://www.blogger.com/video-play.mp4?contentId=fc3f7bdf4fdabe76&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E05
http://www.blogger.com/video-play.mp4?contentId=571e0b4487059e3e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E06
http://www.blogger.com/video-play.mp4?contentId=4baf17a82012940f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E07
http://www.blogger.com/video-play.mp4?contentId=97415784a7279a92&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E08
http://www.blogger.com/video-play.mp4?contentId=c6face8a797613bd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E09
http://www.blogger.com/video-play.mp4?contentId=8e508321aca3d4ed&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E10
http://www.blogger.com/video-play.mp4?contentId=779aec77cb4b1495&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E11
http://www.blogger.com/video-play.mp4?contentId=31300fae1aaba48a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E12
http://www.blogger.com/video-play.mp4?contentId=d418a1e470aab84d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E13
http://www.blogger.com/video-play.mp4?contentId=6aa890eadccab4de&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E14
http://www.blogger.com/video-play.mp4?contentId=4114c75db1a73ccc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E15
http://www.blogger.com/video-play.mp4?contentId=1a6cd580b496683a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/b1t49pm6x/thewalkingdeadse3_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T03|E16
http://www.blogger.com/video-play.mp4?contentId=484b4bb2987e7b30&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E01
http://www.blogger.com/video-play.mp4?contentId=5f496680530c6a91&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E02
http://www.blogger.com/video-play.mp4?contentId=5fe2448b905ae35b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E03
http://www.blogger.com/video-play.mp4?contentId=753e9d0a9b95096d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E04
http://www.blogger.com/video-play.mp4?contentId=3b6e5dc0e9f1a4a5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E05
http://www.blogger.com/video-play.mp4?contentId=17e7290fa5a3c533&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E06
http://www.blogger.com/video-play.mp4?contentId=77ba0e485f5548ed&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E07
http://www.blogger.com/video-play.mp4?contentId=bf003afc0ba7d908&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E08
http://www.blogger.com/video-play.mp4?contentId=61b58af8c3f7c9ca&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E09
http://www.blogger.com/video-play.mp4?contentId=fff0b261b20d5cba&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E10
http://www.blogger.com/video-play.mp4?contentId=10f356b4e019c2da&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E11
http://www.blogger.com/video-play.mp4?contentId=32fb81851909e5a5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E12
http://www.blogger.com/video-play.mp4?contentId=a1ca538813d21a08&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E13
http://www.blogger.com/video-play.mp4?contentId=59bf624d1fa0a1ce&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E14
http://www.blogger.com/video-play.mp4?contentId=80b22ece881e14c3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E15
http://www.blogger.com/video-play.mp4?contentId=b0a46891b4684f4f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/jk2ke1kzt/thewalkingdeadse4_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T04|E16
http://www.blogger.com/video-play.mp4?contentId=3ab29f860216709&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E01
http://www.blogger.com/video-play.mp4?contentId=9bcd580d2a367afc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E02
http://www.blogger.com/video-play.mp4?contentId=5ffcc9dc4fd60940&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E03
http://www.blogger.com/video-play.mp4?contentId=ccf9b5b2b507897&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E04
http://www.blogger.com/video-play.mp4?contentId=8c054b114141c0c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E05
http://www.blogger.com/video-play.mp4?contentId=e7a60a479026502f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E06
http://www.blogger.com/video-play.mp4?contentId=6fb00f1d32c2e9a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E07
http://www.blogger.com/video-play.mp4?contentId=8cc73741eb937936&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E08
http://www.blogger.com/video-play.mp4?contentId=2961a9064af4274e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E09
http://www.blogger.com/video-play.mp4?contentId=699d518a67bc9e8d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E10
http://www.blogger.com/video-play.mp4?contentId=4a50fd35622b0c99&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E11
http://www.blogger.com/video-play.mp4?contentId=d210bb331f3b0734&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E12
http://www.blogger.com/video-play.mp4?contentId=493b6f15d6780012&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E13
http://www.blogger.com/video-play.mp4?contentId=c798b85634fc0f79&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E14
http://www.blogger.com/video-play.mp4?contentId=591ea10df9bd785a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E15
http://www.blogger.com/video-play.mp4?contentId=75a9d259a3fdf374&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/d6dhasvjd/thewalkingdeadse5_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T05|E16
http://www.blogger.com/video-play.mp4?contentId=1f822c5affaaaa6e&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E01
http://www.blogger.com/video-play.mp4?contentId=9c8c81bae48ce41d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E02
http://www.blogger.com/video-play.mp4?contentId=8753ac6e59c28f05&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E03
http://www.blogger.com/video-play.mp4?contentId=4ccfa18ea78fd3c8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E04
http://www.blogger.com/video-play.mp4?contentId=b634ff48fec78d9c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E05
http://www.blogger.com/video-play.mp4?contentId=d960bc0bc0a0c09&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E06
http://www.blogger.com/video-play.mp4?contentId=b416813a9cfdac14&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E07
http://www.blogger.com/video-play.mp4?contentId=6aa128b921310b0c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E08
http://www.blogger.com/video-play.mp4?contentId=8191b77384f8e1fe&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E09
http://www.blogger.com/video-play.mp4?contentId=682f9f8243f8d8b2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E10
http://www.blogger.com/video-play.mp4?contentId=5fa0d7bc1d71990&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E11
http://www.blogger.com/video-play.mp4?contentId=34cf930db6d5b94c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E12
http://www.blogger.com/video-play.mp4?contentId=689ded82c7a59613&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E13
http://www.blogger.com/video-play.mp4?contentId=3396ea5184248c63&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E14
http://www.blogger.com/video-play.mp4?contentId=8046207c14af4b6f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E15
http://www.blogger.com/video-play.mp4?contentId=8a79d4ed8ecd12bd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/rbjad6hll/thewalkingdeadse6_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T06|E16
http://www.blogger.com/video-play.mp4?contentId=c91ce341fa247b9b&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E01
http://www.blogger.com/video-play.mp4?contentId=3df7e366bde4f0ab&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E02
http://www.blogger.com/video-play.mp4?contentId=912d82e6e425f9ff&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E03
http://www.blogger.com/video-play.mp4?contentId=295edac32fbfbb7b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E04
http://www.blogger.com/video-play.mp4?contentId=f2ece6a275888e62&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E05
http://www.blogger.com/video-play.mp4?contentId=76344797c4635803&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E06
http://www.blogger.com/video-play.mp4?contentId=8b1227c66bca288e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E07
http://www.blogger.com/video-play.mp4?contentId=ace421a8564e82c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E08
http://www.blogger.com/video-play.mp4?contentId=ff60120c6527e524&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E09
http://www.blogger.com/video-play.mp4?contentId=a827df1acd1ac19f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E10
http://www.blogger.com/video-play.mp4?contentId=813a765f6006353&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E11
http://www.blogger.com/video-play.mp4?contentId=546279994492d3fc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E12
http://www.blogger.com/video-play.mp4?contentId=eb965e0171282b54&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E13
http://www.blogger.com/video-play.mp4?contentId=66e506f23ac3ba25&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E14
http://www.blogger.com/video-play.mp4?contentId=8b4980d7246ea628&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E15
http://www.blogger.com/video-play.mp4?contentId=7bbcfb7d49782570&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/me5prj56h/thewalkingdeadse7_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T07|E16
http://www.blogger.com/video-play.mp4?contentId=eccfa3da63a3b39e&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E01
http://www.blogger.com/video-play.mp4?contentId=e4667e449fe838b0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E02
http://www.blogger.com/video-play.mp4?contentId=34d08ff2f3214da8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E03
http://www.blogger.com/video-play.mp4?contentId=1462aecffc9214a9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E04
http://www.blogger.com/video-play.mp4?contentId=bafcbab1e039da35&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E05
http://www.blogger.com/video-play.mp4?contentId=c7e17605158b09db&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E06
http://www.blogger.com/video-play.mp4?contentId=5aa9803d40c99036&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E07
http://www.blogger.com/video-play.mp4?contentId=370c3a7f0a25efc9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E08
http://www.blogger.com/video-play.mp4?contentId=9ae157180790b61c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E09
http://www.blogger.com/video-play.mp4?contentId=5351979898417d56&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E10
http://www.blogger.com/video-play.mp4?contentId=165ba25c19ac7ad3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E11
http://www.blogger.com/video-play.mp4?contentId=8deebd0f5c4e2726&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/sf3eolzih/thewalkingdeadse8_MEGAMAXIPTV.jpg" group-title="Série: THE WALKING DEAD",TWD T08|E12
http://www.blogger.com/video-play.mp4?contentId=8b6e4e28b8b26c3f&MEGAMAXIPTV


################################## Série: BIG BANG: A TEORIA ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E01
https://www.blogger.com/video-play.mp4?contentId=ca30777f9a4ce883&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E02
https://www.blogger.com/video-play.mp4?contentId=ca468298dee856fc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E03
https://www.blogger.com/video-play.mp4?contentId=aeca43180016f49b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E04
https://www.blogger.com/video-play.mp4?contentId=e01b6cfe913f41f6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E05
https://www.blogger.com/video-play.mp4?contentId=3e3600ee1be11bcd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E06
https://www.blogger.com/video-play.mp4?contentId=4ebeb1edb15db4c6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E07
https://www.blogger.com/video-play.mp4?contentId=8e58530d48fe2a60&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E08
https://www.blogger.com/video-play.mp4?contentId=200dd7bd1bc4a678&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E09
https://www.blogger.com/video-play.mp4?contentId=a4955a45d9c4eaa3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E10
https://www.blogger.com/video-play.mp4?contentId=ee43bec467feaa8b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E11
https://www.blogger.com/video-play.mp4?contentId=fed5ab00d7dafeae&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E12
https://www.blogger.com/video-play.mp4?contentId=4718b2fbc4dba316&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E13
https://www.blogger.com/video-play.mp4?contentId=a94f3cdda9da9dda&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E14
https://www.blogger.com/video-play.mp4?contentId=584549ab14af6924&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E15
https://www.blogger.com/video-play.mp4?contentId=953b12a4971e627c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E16
https://www.blogger.com/video-play.mp4?contentId=93a186c89af3616a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T01|E17
https://www.blogger.com/video-play.mp4?contentId=20153e6ee97122b1&MEGAMAXIPTV

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E01
https://www.blogger.com/video-play.mp4?contentId=afc975f5af7a4d0c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E02
https://www.blogger.com/video-play.mp4?contentId=8d775a5a3a778299&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E03
https://www.blogger.com/video-play.mp4?contentId=5053e843013eba47&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E04
https://www.blogger.com/video-play.mp4?contentId=f764413106f9deea&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E05
https://www.blogger.com/video-play.mp4?contentId=c4d87a87d1c6e406&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E06
https://www.blogger.com/video-play.mp4?contentId=ddf90b61db166ff0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E07
https://www.blogger.com/video-play.mp4?contentId=defb7ed473bd219b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E08
https://www.blogger.com/video-play.mp4?contentId=ff5f21a059498205&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E09
https://www.blogger.com/video-play.mp4?contentId=a8fbcb776dd01416&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E10
https://www.blogger.com/video-play.mp4?contentId=204c43db5185df3c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E11
https://www.blogger.com/video-play.mp4?contentId=1904ccc7774dbafc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E12
https://www.blogger.com/video-play.mp4?contentId=2f4f5469de7f7560&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E13
https://www.blogger.com/video-play.mp4?contentId=37388721e8bc4a5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E14
https://www.blogger.com/video-play.mp4?contentId=5ab7694d00a6e3e2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E15
https://www.blogger.com/video-play.mp4?contentId=9eaa31b883f13c53&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E16
https://www.blogger.com/video-play.mp4?contentId=bd0635e35627f4bc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E17
https://www.blogger.com/video-play.mp4?contentId=1ed84480d2ec43&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E18
https://www.blogger.com/video-play.mp4?contentId=cb1f80d3e1d6f66e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E19
https://www.blogger.com/video-play.mp4?contentId=f86abc42ea0bf902&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E20
https://www.blogger.com/video-play.mp4?contentId=b4e1242ef9c7aa02&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E21
https://www.blogger.com/video-play.mp4?contentId=8208ceaf8f10ab6e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E22
https://www.blogger.com/video-play.mp4?contentId=36041af3a6f95734&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T02|E23
https://www.blogger.com/video-play.mp4?contentId=4885f3dd09c920fd&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E01
https://www.blogger.com/video-play.mp4?contentId=da09d137afc7663d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E02
https://www.blogger.com/video-play.mp4?contentId=8675cda2623e31b5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E03
https://www.blogger.com/video-play.mp4?contentId=bc867872aa57cfdd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E04
https://www.blogger.com/video-play.mp4?contentId=4fbe1cda6f74ba66&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E05
https://www.blogger.com/video-play.mp4?contentId=77c42541013db9d3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E06
https://www.blogger.com/video-play.mp4?contentId=e0bdbda3bdd2f3a9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E07
https://www.blogger.com/video-play.mp4?contentId=cd6baee1640164db&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E08
https://www.blogger.com/video-play.mp4?contentId=8720e3dda5a6c35c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E09
https://www.blogger.com/video-play.mp4?contentId=dc427a2b7f1c48bd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E10
https://www.blogger.com/video-play.mp4?contentId=9d99b2e030b8037e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E11
https://www.blogger.com/video-play.mp4?contentId=5aa133951a48227d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E12
https://www.blogger.com/video-play.mp4?contentId=51b330e5c2165bdd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E13
https://www.blogger.com/video-play.mp4?contentId=54b08d5fe1648739&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E14
https://www.blogger.com/video-play.mp4?contentId=a365f6abf4def013&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E15
https://www.blogger.com/video-play.mp4?contentId=8c611eba54a8442&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E16
https://www.blogger.com/video-play.mp4?contentId=850189a95db9b61b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E17
https://www.blogger.com/video-play.mp4?contentId=1a644ef7d739ca34&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E18
https://www.blogger.com/video-play.mp4?contentId=8b0e4502a51fa85&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E19
https://www.blogger.com/video-play.mp4?contentId=5ef9fa05d4554635&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E20
https://www.blogger.com/video-play.mp4?contentId=8fab059773d07498&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E21
https://www.blogger.com/video-play.mp4?contentId=d80c206675a7fb79&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E22
https://www.blogger.com/video-play.mp4?contentId=e3b81585b1ecc775&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T03|E23
https://www.blogger.com/video-play.mp4?contentId=4f5ca735cbee4b41&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E01
http://www.blogger.com/video-play.mp4?contentId=4b4245b1567b6f0d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E02
http://www.blogger.com/video-play.mp4?contentId=dee8d0e984a5019c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E03
http://www.blogger.com/video-play.mp4?contentId=536926cb05d0e9b8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E04
http://www.blogger.com/video-play.mp4?contentId=3ab0c1dde46d677&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E05
http://www.blogger.com/video-play.mp4?contentId=8811bf921b648788&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E06
http://www.blogger.com/video-play.mp4?contentId=ae9a974a176dacd2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E07
http://www.blogger.com/video-play.mp4?contentId=228d89802d7f2f94&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E08
http://www.blogger.com/video-play.mp4?contentId=16595360e77855bf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E09
http://www.blogger.com/video-play.mp4?contentId=42d13b95a58560c1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E10
http://www.blogger.com/video-play.mp4?contentId=37eed00d00fa3e4b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E11
http://www.blogger.com/video-play.mp4?contentId=ee5a10c67dfb0dbc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E12
http://www.blogger.com/video-play.mp4?contentId=6f9bf128cdc70a22&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E13
http://www.blogger.com/video-play.mp4?contentId=2d0360bbd76d062&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E14
http://www.blogger.com/video-play.mp4?contentId=3c23c5ca6d24e6a1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E15
http://www.blogger.com/video-play.mp4?contentId=628f869edf0667c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E16
http://www.blogger.com/video-play.mp4?contentId=72afeab82a2f1f27&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E17
http://www.blogger.com/video-play.mp4?contentId=bbe9aca5a8ec9311&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E18
http://www.blogger.com/video-play.mp4?contentId=bdaf38c6e48622fe&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E19
http://www.blogger.com/video-play.mp4?contentId=de84154536751256&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E20
http://www.blogger.com/video-play.mp4?contentId=dd4d96ebdd82d77b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E21
http://www.blogger.com/video-play.mp4?contentId=ee1145573067a117&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E22
http://www.blogger.com/video-play.mp4?contentId=35448cf7054b3183&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E23
http://www.blogger.com/video-play.mp4?contentId=e0d1f8d4546cc91d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T04|E24
http://www.blogger.com/video-play.mp4?contentId=7091927c0271b72d&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E01
http://www.blogger.com/video-play.mp4?contentId=5dd7154f44365871&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E02
http://www.blogger.com/video-play.mp4?contentId=7656230cc7b98a33&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E03
http://www.blogger.com/video-play.mp4?contentId=b421785666498f6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E04
http://www.blogger.com/video-play.mp4?contentId=9e5bc75b642af1e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E05
http://www.blogger.com/video-play.mp4?contentId=dd7c727d418ceb6f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E06
http://www.blogger.com/video-play.mp4?contentId=b47fbe04ef08db59&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E07
http://www.blogger.com/video-play.mp4?contentId=eef60d59ed0facf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E08
http://www.blogger.com/video-play.mp4?contentId=a0cd4fc6aafa64c3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E09
http://www.blogger.com/video-play.mp4?contentId=71edff910f113be3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E10
http://www.blogger.com/video-play.mp4?contentId=10166383e5ed2a5a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E11
http://www.blogger.com/video-play.mp4?contentId=ab7c5167f4914f4c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E12
http://www.blogger.com/video-play.mp4?contentId=74163c4fe4d5edca&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E13
http://www.blogger.com/video-play.mp4?contentId=c03f18b64171793f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E14
http://www.blogger.com/video-play.mp4?contentId=63309480a66d04cd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E15
http://www.blogger.com/video-play.mp4?contentId=2f26dc76a2caf819&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E16
http://www.blogger.com/video-play.mp4?contentId=3c68ca373d0987c8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E17
http://www.blogger.com/video-play.mp4?contentId=c80b5396ffff369b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E18
http://www.blogger.com/video-play.mp4?contentId=6a44f8325d07ed09&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E19
http://www.blogger.com/video-play.mp4?contentId=d7d74b59bb3fa31b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E20
http://www.blogger.com/video-play.mp4?contentId=57976167e5a8d83e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E21
http://www.blogger.com/video-play.mp4?contentId=1f04b7fb5b435d0a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E22
http://www.blogger.com/video-play.mp4?contentId=fdb17ef22e56571f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E23
http://www.blogger.com/video-play.mp4?contentId=930bcf5ddc39a492&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T05|E24
http://www.blogger.com/video-play.mp4?contentId=cfd783ec5f1131cc&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E01
http://www.blogger.com/video-play.mp4?contentId=56109b76f54ba4c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E02
http://www.blogger.com/video-play.mp4?contentId=81ef6cae94b1375d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E03
http://www.blogger.com/video-play.mp4?contentId=e03ee4f867f293a3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E04
http://www.blogger.com/video-play.mp4?contentId=9defa58716d188a6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E05
http://www.blogger.com/video-play.mp4?contentId=ce26eebc23de4846&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E06
http://www.blogger.com/video-play.mp4?contentId=aa35a58b03a3a748&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E07
http://www.blogger.com/video-play.mp4?contentId=b07b52bb7f59f643&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E08
http://www.blogger.com/video-play.mp4?contentId=2b36f28abd8c4b1a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E09
http://www.blogger.com/video-play.mp4?contentId=45cf46328d5638a4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E10
http://www.blogger.com/video-play.mp4?contentId=e19540e21b419ba1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E11
http://www.blogger.com/video-play.mp4?contentId=71bee1ce7cfe4124&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E12
http://www.blogger.com/video-play.mp4?contentId=3f0535b4180f10a6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E13
http://www.blogger.com/video-play.mp4?contentId=942d78174fbdc135&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E14
http://www.blogger.com/video-play.mp4?contentId=752aa604e2ce8b29&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E15
http://www.blogger.com/video-play.mp4?contentId=16af2fb3cfb63667&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E16
http://www.blogger.com/video-play.mp4?contentId=3ab0aca448fdb4a5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E17
http://www.blogger.com/video-play.mp4?contentId=d685486d6c10ddf5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E18
http://www.blogger.com/video-play.mp4?contentId=a0a93786d35a7991&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E19
http://www.blogger.com/video-play.mp4?contentId=c8eaecde1c16cca7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E20
http://www.blogger.com/video-play.mp4?contentId=5deeacde6a3c94ae&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E21
http://www.blogger.com/video-play.mp4?contentId=6e953ad36f443825&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E22
http://www.blogger.com/video-play.mp4?contentId=4b9c097a14896cf8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E23
http://www.blogger.com/video-play.mp4?contentId=c352a5f011120e21&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T06|E24
http://www.blogger.com/video-play.mp4?contentId=9cb65ad1c41b137f&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E01
http://www.blogger.com/video-play.mp4?contentId=488bfcca9945fc18&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E02
http://www.blogger.com/video-play.mp4?contentId=84ef8ba9057ea443&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E03
http://www.blogger.com/video-play.mp4?contentId=d2a8c40393bed0c2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E04
http://www.blogger.com/video-play.mp4?contentId=7f77f94e8278d32c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E05
http://www.blogger.com/video-play.mp4?contentId=dac1e6ec64fc0af3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E06
http://www.blogger.com/video-play.mp4?contentId=7cc01d44ce5c4463&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E07
http://www.blogger.com/video-play.mp4?contentId=bc5dc9ff6658d81b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E08
http://www.blogger.com/video-play.mp4?contentId=4110230e62d82609&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E09
http://www.blogger.com/video-play.mp4?contentId=ccee67092f2b3ca9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E10
http://www.blogger.com/video-play.mp4?contentId=50e3b69b9c312399&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E11
http://www.blogger.com/video-play.mp4?contentId=9cad3b69b9fa992c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E12
http://www.blogger.com/video-play.mp4?contentId=fc086e2667ed0d55&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E13
http://www.blogger.com/video-play.mp4?contentId=3890d9a958a8b417&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E14
http://www.blogger.com/video-play.mp4?contentId=a5da0d3ad3d5389a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E15
http://www.blogger.com/video-play.mp4?contentId=f2362378b2982e69&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E16
http://www.blogger.com/video-play.mp4?contentId=e93ee1a3db7e470a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E17
http://www.blogger.com/video-play.mp4?contentId=19582dc26568865f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E18
http://www.blogger.com/video-play.mp4?contentId=169b489fb9412b8b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E19
http://www.blogger.com/video-play.mp4?contentId=69001aa5985bc9cb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E20
http://www.blogger.com/video-play.mp4?contentId=68e9ceace41962fe&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E21
http://www.blogger.com/video-play.mp4?contentId=24e80b43d9ec711f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E22
http://www.blogger.com/video-play.mp4?contentId=db5edac2b99d9010&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E23
http://www.blogger.com/video-play.mp4?contentId=45a9d6203a154112&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T07|E24
http://www.blogger.com/video-play.mp4?contentId=fac07e5e988eb7a6&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E01
http://www.blogger.com/video-play.mp4?contentId=7babba6a315bbc82&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E02
https://www.blogger.com/video-play.mp4?contentId=4b044ecf70e2c75a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E03
http://www.blogger.com/video-play.mp4?contentId=db85ecf0bb4031b4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E04
http://www.blogger.com/video-play.mp4?contentId=2a842b948962a3ac&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E05
http://www.blogger.com/video-play.mp4?contentId=41b6f1bbbf8ee102&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E06
http://www.blogger.com/video-play.mp4?contentId=6967b545c88eca96&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E07
http://www.blogger.com/video-play.mp4?contentId=d16d2aaa8df4c75a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E08
http://www.blogger.com/video-play.mp4?contentId=fe37f3f4c2f1b46d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E09
http://www.blogger.com/video-play.mp4?contentId=797c01745e1f8f2d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E10
http://www.blogger.com/video-play.mp4?contentId=fc4d30d0fa9f360c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E11
http://www.blogger.com/video-play.mp4?contentId=93c0309f3ce02007&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E12
http://www.blogger.com/video-play.mp4?contentId=f74ab9096963b1a6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E13
http://www.blogger.com/video-play.mp4?contentId=2b716aa52a860586&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E14
http://www.blogger.com/video-play.mp4?contentId=ec5017dd84b254b1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E15
http://www.blogger.com/video-play.mp4?contentId=b6e7cf24481575dd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E16
http://www.blogger.com/video-play.mp4?contentId=eae7254cb34b685c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E17
http://www.blogger.com/video-play.mp4?contentId=f4e1fa1f50f39a79&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E18
http://www.blogger.com/video-play.mp4?contentId=48344543f726c49&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E19
http://www.blogger.com/video-play.mp4?contentId=4654b5927a24f528&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E20
http://www.blogger.com/video-play.mp4?contentId=bd50c1766c005aa4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E21
http://www.blogger.com/video-play.mp4?contentId=428e1dacf7bae954&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E22
http://www.blogger.com/video-play.mp4?contentId=df97d114b031a86c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E23
http://www.blogger.com/video-play.mp4?contentId=6476eee36c8c7454&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T08|E24
http://www.blogger.com/video-play.mp4?contentId=b1cfe321f9dd13be



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E01
https://www.blogger.com/video-play.mp4?contentId=2b06a9ecff4c705a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E02
https://www.blogger.com/video-play.mp4?contentId=9c99c72340810715&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E03
https://www.blogger.com/video-play.mp4?contentId=dc052c23b7a52172&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E04
https://www.blogger.com/video-play.mp4?contentId=42aae42eff299cec&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E05
https://www.blogger.com/video-play.mp4?contentId=4afa58ebccf94266&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E06
https://www.blogger.com/video-play.mp4?contentId=f097a647e3fc2845&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E07
https://www.blogger.com/video-play.mp4?contentId=b1d06f110d10463c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E08
https://www.blogger.com/video-play.mp4?contentId=f2103cf1ef0a296d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E09
https://www.blogger.com/video-play.mp4?contentId=fcb467234ef1dc53&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E10
https://www.blogger.com/video-play.mp4?contentId=2a3925fc1878665d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E11
https://www.blogger.com/video-play.mp4?contentId=be055d008973bc4c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E12
https://www.blogger.com/video-play.mp4?contentId=9150bd765d6db3a4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E13
https://www.blogger.com/video-play.mp4?contentId=11f85b7bbc17a255&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E14
https://www.blogger.com/video-play.mp4?contentId=8cd7e48bf7d5ee84&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E15
https://www.blogger.com/video-play.mp4?contentId=7cfaaf9e3cb0b267&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E16
https://www.blogger.com/video-play.mp4?contentId=107a38478f0c6d01&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E17
https://www.blogger.com/video-play.mp4?contentId=5f6e3acf0d693511&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E18
https://www.blogger.com/video-play.mp4?contentId=4ae5927bc6318a6f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E19
https://www.blogger.com/video-play.mp4?contentId=c77ea842f1ce438a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E20
https://www.blogger.com/video-play.mp4?contentId=95a5a17dcff53e35&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E21
https://www.blogger.com/video-play.mp4?contentId=182e44b17077857&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E22
https://www.blogger.com/video-play.mp4?contentId=95a3191b2e259cb5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E23
https://www.blogger.com/video-play.mp4?contentId=8cd078939091f315&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T09|E24
https://www.blogger.com/video-play.mp4?contentId=2120da827db0f26e&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E01
http://www.blogger.com/video-play.mp4?contentId=f7945c7844050ca2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E02
http://www.blogger.com/video-play.mp4?contentId=b0ed9f0f303d2f6d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E03
http://www.blogger.com/video-play.mp4?contentId=60436030406c371b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E04
http://www.blogger.com/video-play.mp4?contentId=130fa550be21615d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E05
http://www.blogger.com/video-play.mp4?contentId=39a01fd78697e55&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E06
http://www.blogger.com/video-play.mp4?contentId=e97fdbc1f645ba02&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E07
http://www.blogger.com/video-play.mp4?contentId=fbdc4215a0f29833&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E08
https://www.blogger.com/video-play.mp4?contentId=9408b0ef3fbd6f7b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E09
https://www.blogger.com/video-play.mp4?contentId=e692b9b56f635bdd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E10
https://www.blogger.com/video-play.mp4?contentId=7532663f0b6d99ba&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E11
https://www.blogger.com/video-play.mp4?contentId=a14427afbabdfe6c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E12
https://www.blogger.com/video-play.mp4?contentId=7ff006c3ae393222&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E13
https://www.blogger.com/video-play.mp4?contentId=2af7787049cfa337&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E14
https://www.blogger.com/video-play.mp4?contentId=6dc83fbd0729c0e2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E15
https://www.blogger.com/video-play.mp4?contentId=1d7d1d6369353203&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E16
https://www.blogger.com/video-play.mp4?contentId=e4d50c0a7dfbe894&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E17
https://www.blogger.com/video-play.mp4?contentId=5394d3d6629cdaa3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E18
https://www.blogger.com/video-play.mp4?contentId=4e55a021d5adac17&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E19
https://www.blogger.com/video-play.mp4?contentId=2cf954f8bc6801ab&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E20
https://www.blogger.com/video-play.mp4?contentId=12db22e3581d2e76&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E21
https://www.blogger.com/video-play.mp4?contentId=c350904043545a71&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E22
https://www.blogger.com/video-play.mp4?contentId=f026912fe0912ef4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E23
https://www.blogger.com/video-play.mp4?contentId=5ea630c5b03f8ed1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T10|E24
https://www.blogger.com/video-play.mp4?contentId=f8b69a4e4222a36e&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E01
https://www.blogger.com/video-play.mp4?contentId=8e05f9686987bfd5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E02
https://www.blogger.com/video-play.mp4?contentId=5caa9bf264a5fae&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E03
https://www.blogger.com/video-play.mp4?contentId=e3fee4c0236e81ff&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E04
https://www.blogger.com/video-play.mp4?contentId=fbd30bffdfd878bb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E05
https://www.blogger.com/video-play.mp4?contentId=a1eb2ebd27ad549e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E06
https://www.blogger.com/video-play.mp4?contentId=80c0974f10dad8c6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E07
https://www.blogger.com/video-play.mp4?contentId=b3b13d1c2542de63&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E08
https://www.blogger.com/video-play.mp4?contentId=c65d5f55b033e0cd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E09
https://www.blogger.com/video-play.mp4?contentId=5df596266b767db7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E10
https://www.blogger.com/video-play.mp4?contentId=c11643f79f08a884&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E11
https://www.blogger.com/video-play.mp4?contentId=7b4bb8ed227fb294&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E12
https://www.blogger.com/video-play.mp4?contentId=3a2c23fc4adb779&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E13
https://www.blogger.com/video-play.mp4?contentId=c8e775efa214083e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série: BIG BANG: A TEORIA ",BBAT T11|E14
https://www.blogger.com/video-play.mp4?contentId=9ccedfb0435d7542&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4nhmamlnd/bigbangateoriase1_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T01|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/01dub/17-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/hrn6nbt4p/bigbangateoriase2_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T02|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/02dub/23-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/h24eaz0ax/bigbangateoriase3_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T03|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/03dub/23-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/lb94d4o4p/bigbangateoriase4_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T04|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/04dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllym7h5/bigbangateoriase5_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T05|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/05dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/v8k567ybd/bigbangateoriase6_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T06|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/06dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/74tdhx84p/bigbangateoriase7_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T07|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/07dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E19
Vhttp://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/y2najo87d/bigbangateoriase8_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T08|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/08dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/exk19x195/bigbangateoriase9_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T09|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/09dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/14-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E15
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/15-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E16
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/16-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E17
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/17-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E18
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/18-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E19
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/19-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E20
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/20-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E21
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/21-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E22
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/22-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E23
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/23-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w1o6h/bigbangateoriase10_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T10|E24
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/10dub/24-ALTO.mp4?MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E01
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/01-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E02
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/02-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E03
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/03-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E04
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/04-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E05
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/05-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E06
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/06-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E07
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/07-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E08
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/08-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E09
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/09-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E10
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/10-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E11
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/11-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E12
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/12-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E13
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/13-ALTO.mp4?MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/3xytybxzd/bigbangateoriase110_MEGAMAXIPTV.jpg" group-title="Série HD: BIG BANG: A TEORIA ",BBAT (HD) T11|E14
http://cdn1.ntcdn.us/content/sobdemanda/netcine-bucket/tbbt/11dub/14-ALTO.mp4?MEGAMAXIPTV


################################## Série: SUPERNATURAL ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E01
https://www.blogger.com/video-play.mp4?contentId=3151521e0c6f85fd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E02
https://www.blogger.com/video-play.mp4?contentId=62051b3cc6eb996f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E03
https://www.blogger.com/video-play.mp4?contentId=d65060be09c47473&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E04
https://www.blogger.com/video-play.mp4?contentId=e5f6abcb358afb90&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E05
https://www.blogger.com/video-play.mp4?contentId=c551a95a5f151df9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E06
https://www.blogger.com/video-play.mp4?contentId=7fe2ac7640cd712e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E07
https://www.blogger.com/video-play.mp4?contentId=e93d67cc67f46d03&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E08
https://www.blogger.com/video-play.mp4?contentId=34f87e9ac4eb5324&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E09
https://www.blogger.com/video-play.mp4?contentId=9fac67176c50a9cf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E10
https://www.blogger.com/video-play.mp4?contentId=111486fa9cbf538c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E11
https://www.blogger.com/video-play.mp4?contentId=a576a9c58623c44b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E12
https://www.blogger.com/video-play.mp4?contentId=43cb089e03ef7f3a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E13
https://www.blogger.com/video-play.mp4?contentId=ee17135e85d19f2d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E14
https://www.blogger.com/video-play.mp4?contentId=95db0cc5f387c749&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E15
https://www.blogger.com/video-play.mp4?contentId=d48177c7e948a198&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E16
https://www.blogger.com/video-play.mp4?contentId=df6c93727ef22e96&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E17
https://www.blogger.com/video-play.mp4?contentId=cb0ff8ff5545c4cf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E18
https://www.blogger.com/video-play.mp4?contentId=22dbe7b2d8a41c92&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E19
https://www.blogger.com/video-play.mp4?contentId=c6139ec32a1acead&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E20
https://www.blogger.com/video-play.mp4?contentId=22b6eade1061e5dc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E21
https://www.blogger.com/video-play.mp4?contentId=39b67a5fe45a87b2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/4aq84inyx/supernaturalse1_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T01|E22
https://www.blogger.com/video-play.mp4?contentId=d2c8ace7a6aa0002&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E01
https://www.blogger.com/video-play.mp4?contentId=8b762e9b8db1d829&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E02
https://www.blogger.com/video-play.mp4?contentId=c84404898ab5d772&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E03
https://www.blogger.com/video-play.mp4?contentId=c1556c152c4ad777&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E04
https://www.blogger.com/video-play.mp4?contentId=ff52c43c8636caf3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E05
https://www.blogger.com/video-play.mp4?contentId=960f3ed1ab9f39c9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E06
https://www.blogger.com/video-play.mp4?contentId=5a67e4541d90b6e1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E07
https://www.blogger.com/video-play.mp4?contentId=7cc75dcffd3f82e6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E08
https://www.blogger.com/video-play.mp4?contentId=313c3beea4bc7f1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E09
https://www.blogger.com/video-play.mp4?contentId=de5e2956a37b6200&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E10
https://www.blogger.com/video-play.mp4?contentId=f7d26fe28e93525f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E11
https://www.blogger.com/video-play.mp4?contentId=86f2a290a36e6e08&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E12
https://www.blogger.com/video-play.mp4?contentId=267aafda9bdc4463&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E13
https://www.blogger.com/video-play.mp4?contentId=f85128e60be5cbbf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E14
https://www.blogger.com/video-play.mp4?contentId=dcd9c560257dd43d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E15
https://www.blogger.com/video-play.mp4?contentId=1e22998fdec24a32&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E16
https://www.blogger.com/video-play.mp4?contentId=2fb17d8f31b003c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E17
https://www.blogger.com/video-play.mp4?contentId=e976dc021be12d74&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E18
https://www.blogger.com/video-play.mp4?contentId=a436ed11e72b80eb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E19
https://www.blogger.com/video-play.mp4?contentId=89cc68df20583bf7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E20
https://www.blogger.com/video-play.mp4?contentId=9c69ff0b2358c928&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E21
https://www.blogger.com/video-play.mp4?contentId=ad9f883ebbb8ec54&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/gcllyo4x5/supernaturalse2_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T02|E22
https://www.blogger.com/video-play.mp4?contentId=72b15259d886989c&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E01
https://www.blogger.com/video-play.mp4?contentId=46537a719d442e7e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E02
https://www.blogger.com/video-play.mp4?contentId=c53ad868bc0ca8e6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E03
https://www.blogger.com/video-play.mp4?contentId=b8d41792b2d3d993&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E04
https://www.blogger.com/video-play.mp4?contentId=c2fbe0c266e8be87&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E05
https://www.blogger.com/video-play.mp4?contentId=3c4c28e987048511&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E06
https://www.blogger.com/video-play.mp4?contentId=b345226d2803d4ab&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E07
https://www.blogger.com/video-play.mp4?contentId=fccb5d29d869afb5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E08
https://www.blogger.com/video-play.mp4?contentId=ae2e42d5575e6d1a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E09
https://www.blogger.com/video-play.mp4?contentId=79ba9c4dc2561db2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E10
https://www.blogger.com/video-play.mp4?contentId=b29bba9bfc3ffb62&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E11
https://www.blogger.com/video-play.mp4?contentId=eadfa1c9290820d5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E12
https://www.blogger.com/video-play.mp4?contentId=e297fbcd2d0d9c1e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E13
https://www.blogger.com/video-play.mp4?contentId=ffb6d3e4920166fe&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E14
https://www.blogger.com/video-play.mp4?contentId=c087efee9ab797bb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E15
https://www.blogger.com/video-play.mp4?contentId=ede21685f21e5865&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/px58lk1yx/supernaturalse3_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T03|E16
https://www.blogger.com/video-play.mp4?contentId=b9371aaa82c89169&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E01
https://www.blogger.com/video-play.mp4?contentId=6412827cdae9c0de&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E02
https://www.blogger.com/video-play.mp4?contentId=f7ad227ea7aa7650&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E03
https://www.blogger.com/video-play.mp4?contentId=ed030bb5d31cf2df&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E04
https://www.blogger.com/video-play.mp4?contentId=c287df4d0f1251ff&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E05
https://www.blogger.com/video-play.mp4?contentId=7202f3c95bb593a5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E06
https://www.blogger.com/video-play.mp4?contentId=603bfbceafc557e2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E07
https://www.blogger.com/video-play.mp4?contentId=e9f2e9acdbe20866&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E08
https://www.blogger.com/video-play.mp4?contentId=969d4a3aff2edc13&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E09
https://www.blogger.com/video-play.mp4?contentId=1fb20a1a3f2f0853&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E10
https://www.blogger.com/video-play.mp4?contentId=d5ae3384b4b41a6f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E11
https://www.blogger.com/video-play.mp4?contentId=b1c52f730dd3c363&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E12
https://www.blogger.com/video-play.mp4?contentId=e435429fb19df845&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E13
https://www.blogger.com/video-play.mp4?contentId=78265d8edc746c72&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E14
https://www.blogger.com/video-play.mp4?contentId=72bb24d8e7661901&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E15
https://www.blogger.com/video-play.mp4?contentId=d81ecc75eccfe42f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E16
https://www.blogger.com/video-play.mp4?contentId=440aac49f3d15c0b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E17
https://www.blogger.com/video-play.mp4?contentId=ce77137a2263da3b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E18
https://www.blogger.com/video-play.mp4?contentId=6892ba0206286cf3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E19
https://www.blogger.com/video-play.mp4?contentId=99cb28828e918fec&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E20
https://www.blogger.com/video-play.mp4?contentId=11c81c0a4e7a1399&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E21
https://www.blogger.com/video-play.mp4?contentId=37c60cb421ca8a8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ys62w2yh5/supernaturalse4_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T04|E22
https://www.blogger.com/video-play.mp4?contentId=2e1680fb3d694d47&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E01
https://www.blogger.com/video-play.mp4?contentId=1a1cbbad2f779cd7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E02
https://www.blogger.com/video-play.mp4?contentId=9b5558f5d4b01676&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E03
https://www.blogger.com/video-play.mp4?contentId=978ef21ebcd91d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E04
https://www.blogger.com/video-play.mp4?contentId=f6b2f5359468ebe7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E05
https://www.blogger.com/video-play.mp4?contentId=ec026f3fbfb8274f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E06
https://www.blogger.com/video-play.mp4?contentId=f6d81935f78f211b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E07
https://www.blogger.com/video-play.mp4?contentId=f989cf338d82399f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E08
https://www.blogger.com/video-play.mp4?contentId=492921b8bc5838fb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E09
https://www.blogger.com/video-play.mp4?contentId=307042fb8dce88af&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E10
https://www.blogger.com/video-play.mp4?contentId=24873fc7dbbb0d0a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E11
https://www.blogger.com/video-play.mp4?contentId=392331f50cf95823&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E12
https://www.blogger.com/video-play.mp4?contentId=2318077a2b22d1b4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E13
https://www.blogger.com/video-play.mp4?contentId=def40d3856fd5d84&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E14
https://www.blogger.com/video-play.mp4?contentId=274b7d05e16647a7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E15
https://www.blogger.com/video-play.mp4?contentId=cd7f187f238c65e0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E16
https://www.blogger.com/video-play.mp4?contentId=2b472edc23f6c83f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E17
https://www.blogger.com/video-play.mp4?contentId=457b7e561dd92705&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E18
https://www.blogger.com/video-play.mp4?contentId=7e0713a46b1d77f1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E19
https://www.blogger.com/video-play.mp4?contentId=4821647146858ffd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E20
https://www.blogger.com/video-play.mp4?contentId=74c21fdb1b88290f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E21
https://www.blogger.com/video-play.mp4?contentId=9059af3979c90440&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/vlbjcgtgp/supernaturalse5_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T05|E22
https://www.blogger.com/video-play.mp4?contentId=49df266c7f35bda5&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E01
https://www.blogger.com/video-play.mp4?contentId=49d4f603b1ac0849&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E02
https://www.blogger.com/video-play.mp4?contentId=5ec1e4bf10046ca&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E03
https://www.blogger.com/video-play.mp4?contentId=c78def1ae993af7d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E04
https://www.blogger.com/video-play.mp4?contentId=f979549a46f36f2f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E05
https://www.blogger.com/video-play.mp4?contentId=e63a6522d10e6d7b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E06
https://www.blogger.com/video-play.mp4?contentId=ff31363b8ccf3185&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E07
https://www.blogger.com/video-play.mp4?contentId=40d0c135f52599ed&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E08
https://www.blogger.com/video-play.mp4?contentId=8ec388ab25028661&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E09
https://www.blogger.com/video-play.mp4?contentId=b536262ce4dcec70&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E10
https://www.blogger.com/video-play.mp4?contentId=922caafd8d76cf1d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E11
https://www.blogger.com/video-play.mp4?contentId=959f0fc4ad3824e9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E12
https://www.blogger.com/video-play.mp4?contentId=3361b19a43303418&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E13
https://www.blogger.com/video-play.mp4?contentId=f48e407a900bf54f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E14
https://www.blogger.com/video-play.mp4?contentId=b848955d9f120961&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E15
https://www.blogger.com/video-play.mp4?contentId=42e11f19be755322&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E16
https://www.blogger.com/video-play.mp4?contentId=b130b9f956ab1c17&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E17
https://www.blogger.com/video-play.mp4?contentId=c442d24d87b41a8c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E18
https://www.blogger.com/video-play.mp4?contentId=fcc7381d2a14907a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E19
https://www.blogger.com/video-play.mp4?contentId=35758425bd45a2a6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E20
https://www.blogger.com/video-play.mp4?contentId=240a675b47d9b6f8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E21
https://www.blogger.com/video-play.mp4?contentId=85cabb3c60a128e5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/q9wmrqzo9/supernaturalse6_MEGAMAXIPTV.jpgZ" group-title="Série: SUPERNATURAL",SUPERNATURAL T06|E22
https://www.blogger.com/video-play.mp4?contentId=fcb76be55b9ce75c&MEGAMAXIPTV



#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E01
https://www.blogger.com/video-play.mp4?contentId=2e02bd0ea1ccf3da&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E02
https://www.blogger.com/video-play.mp4?contentId=56802975a0eaed1e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E03
https://www.blogger.com/video-play.mp4?contentId=e3c7c1cf7d839b04&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E04
https://www.blogger.com/video-play.mp4?contentId=c1881639ade94b08&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E05
https://www.blogger.com/video-play.mp4?contentId=39777eeaf43d63de&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E06
https://www.blogger.com/video-play.mp4?contentId=7c6dae3bd78b2476&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E07
https://www.blogger.com/video-play.mp4?contentId=cb647dc556ac368b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E08
https://www.blogger.com/video-play.mp4?contentId=7e4240dbb4e94b32&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E09
https://www.blogger.com/video-play.mp4?contentId=3298f10a173c50ae&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E10
https://www.blogger.com/video-play.mp4?contentId=8ccb92669a303df5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E11
https://www.blogger.com/video-play.mp4?contentId=48c03a622eae8249&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E12
https://www.blogger.com/video-play.mp4?contentId=d1241878bdc4a5d2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E13
https://www.blogger.com/video-play.mp4?contentId=6a8bac783c8d5c9e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E14
https://www.blogger.com/video-play.mp4?contentId=94a5845fe666f2c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E15
https://www.blogger.com/video-play.mp4?contentId=5e37026de9080a1c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E16
https://www.blogger.com/video-play.mp4?contentId=bcbc70ff9d65b815&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E17
https://www.blogger.com/video-play.mp4?contentId=815aa8184b7b37ce&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E18
https://www.blogger.com/video-play.mp4?contentId=83be605bfefcd740&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E19
https://www.blogger.com/video-play.mp4?contentId=100e0ceb057549cd&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E20
https://www.blogger.com/video-play.mp4?contentId=8f72215fbc74e07f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E21
https://www.blogger.com/video-play.mp4?contentId=881a954b5ff98b6a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E22
https://www.blogger.com/video-play.mp4?contentId=ef7eee858d0dd2b5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/tgr6bdzjt/supernaturalse7_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T07|E23
https://www.blogger.com/video-play.mp4?contentId=14f35eb3b5397123&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E01
https://www.blogger.com/video-play.mp4?contentId=e6c0cdc7f6a95ea4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E02
https://www.blogger.com/video-play.mp4?contentId=c5206abcc78ca0b7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E03
https://www.blogger.com/video-play.mp4?contentId=79e33e54dc00d5ad&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E04
https://www.blogger.com/video-play.mp4?contentId=3bf58eb3592a5dff&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E05
https://www.blogger.com/video-play.mp4?contentId=8675b4ab4ca5fd5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E06
https://www.blogger.com/video-play.mp4?contentId=88a015ac4e532553&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E07
https://www.blogger.com/video-play.mp4?contentId=3a12af6f596df534&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E08
https://www.blogger.com/video-play.mp4?contentId=29fd1a13adfd4b3f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E09
https://www.blogger.com/video-play.mp4?contentId=7fd13a8a4a2c39e4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E10
https://www.blogger.com/video-play.mp4?contentId=ee5cd9c259954460&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E11
https://www.blogger.com/video-play.mp4?contentId=293aa987b621e1eb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E12
https://www.blogger.com/video-play.mp4?contentId=2b4f869bd9b887a9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E13
https://www.blogger.com/video-play.mp4?contentId=22b59a2e92db5dcf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E14
https://www.blogger.com/video-play.mp4?contentId=cdbd2520dee6bd24&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E15
https://www.blogger.com/video-play.mp4?contentId=d7538d252ef5556&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E16
https://www.blogger.com/video-play.mp4?contentId=d5fa269590c222e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E17
https://www.blogger.com/video-play.mp4?contentId=c56c2e8fdf2c22e4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E18
https://www.blogger.com/video-play.mp4?contentId=7ae955e69f037da8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E19
https://www.blogger.com/video-play.mp4?contentId=374006f865e1315f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E20
https://www.blogger.com/video-play.mp4?contentId=f6681ce836a659c9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E21
https://www.blogger.com/video-play.mp4?contentId=431c9c73bae68c69&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E22
https://www.blogger.com/video-play.mp4?contentId=92ab9f8c23569bdc&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/n4c10yoll/supernaturalse8_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T08|E23
https://www.blogger.com/video-play.mp4?contentId=77259d6ee3abbb0&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E01
https://www.blogger.com/video-play.mp4?contentId=dfccb5a0d63109e9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E02
https://www.blogger.com/video-play.mp4?contentId=1cd722d4f037e733&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E03
https://www.blogger.com/video-play.mp4?contentId=ce09adc0e874ad9a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E04
https://www.blogger.com/video-play.mp4?contentId=ec2a2578f08f6575&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E05
https://www.blogger.com/video-play.mp4?contentId=efd98c71193e3948&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E06
https://www.blogger.com/video-play.mp4?contentId=a4e3c34dd4f4b5a7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E07
https://www.blogger.com/video-play.mp4?contentId=b72ae3d1d6759d8e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E08
https://www.blogger.com/video-play.mp4?contentId=b32c2856bb477360&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E09
https://www.blogger.com/video-play.mp4?contentId=2871b6caf32b70ce&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E10
https://www.blogger.com/video-play.mp4?contentId=ee54ffcc92262a41&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E11
https://www.blogger.com/video-play.mp4?contentId=742bc28b7cf7ac64&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E12
https://www.blogger.com/video-play.mp4?contentId=128c0aedb7530e46&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E13
https://www.blogger.com/video-play.mp4?contentId=13ecaa8b0879ff9b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E14
https://www.blogger.com/video-play.mp4?contentId=2c33877036ff386&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E15
https://www.blogger.com/video-play.mp4?contentId=6398929ff104848d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E16
https://www.blogger.com/video-play.mp4?contentId=3510378a0049ee62&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E17
https://www.blogger.com/video-play.mp4?contentId=e05f8167679c2c11&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E18
https://www.blogger.com/video-play.mp4?contentId=a22182bb7b43dfad&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E19
https://www.blogger.com/video-play.mp4?contentId=fb3eb5a18a4969f0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E20
https://www.blogger.com/video-play.mp4?contentId=eed30cb2aa027d13&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E21
https://www.blogger.com/video-play.mp4?contentId=aaffb68e3b587d9a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E22
https://www.blogger.com/video-play.mp4?contentId=f761dfa6a7417e70&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/5eacfxy61/supernaturalse9_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T09|E23
https://www.blogger.com/video-play.mp4?contentId=cfa91f7d6f44f747&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E(ESPECIAL)
https://www.blogger.com/video-play.mp4?contentId=d3cd8d1fd45c80c5&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E01
https://www.blogger.com/video-play.mp4?contentId=510a4f23aa91c4fb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E02
https://www.blogger.com/video-play.mp4?contentId=f23fab7b861a2d3b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E03
https://www.blogger.com/video-play.mp4?contentId=cd95f1026b0fdd0e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E04
https://www.blogger.com/video-play.mp4?contentId=b6a802ec138d17ee&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E05
https://www.blogger.com/video-play.mp4?contentId=d2cf79fe134b9a4b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E06
https://www.blogger.com/video-play.mp4?contentId=2d92e32d45569567&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E07
https://www.blogger.com/video-play.mp4?contentId=43db8c6e25c59559&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E08
https://www.blogger.com/video-play.mp4?contentId=3f11e1fb96ce36a0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E09
https://www.blogger.com/video-play.mp4?contentId=10906811029f90b0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E10
https://www.blogger.com/video-play.mp4?contentId=bb7d78f113b1b1d8&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E11
https://www.blogger.com/video-play.mp4?contentId=2f06a15a99e6006a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E12
https://www.blogger.com/video-play.mp4?contentId=2f54085f7f67358f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E13
https://www.blogger.com/video-play.mp4?contentId=b9646c4d2f01cdb3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E14
https://www.blogger.com/video-play.mp4?contentId=9416d5b19e7a37c1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E15
https://www.blogger.com/video-play.mp4?contentId=a67157b722008339&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E16
https://www.blogger.com/video-play.mp4?contentId=f85e99114b9930f6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E17
https://www.blogger.com/video-play.mp4?contentId=687c8bb4c32998ed&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E18
https://www.blogger.com/video-play.mp4?contentId=747de7adc3b6ab99&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E19
https://www.blogger.com/video-play.mp4?contentId=ec86fc5c7129bee0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E20
https://www.blogger.com/video-play.mp4?contentId=8a1797ce5e60ae96&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E21
https://www.blogger.com/video-play.mp4?contentId=2e151cd3436e813&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E22
https://www.blogger.com/video-play.mp4?contentId=5c3320539532a5a9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/app90nryh/supernaturalse10_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T10|E23
https://www.blogger.com/video-play.mp4?contentId=7626135b19c57a27&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E01
https://www.blogger.com/video-play.mp4?contentId=409b62a0e043ea1&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E02
https://www.blogger.com/video-play.mp4?contentId=4f67ccc64600df9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E03
https://www.blogger.com/video-play.mp4?contentId=db69460199d72e64&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E04
https://www.blogger.com/video-play.mp4?contentId=75ff9b4d8f5520c7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E05
https://www.blogger.com/video-play.mp4?contentId=8161577d3abd3a5f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E06
https://www.blogger.com/video-play.mp4?contentId=9925952aafb72c4c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E07
https://www.blogger.com/video-play.mp4?contentId=3b2035ee5c439a40&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E08
https://www.blogger.com/video-play.mp4?contentId=66057879ab90b726&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E09
https://www.blogger.com/video-play.mp4?contentId=6dca4c0cff3204ae&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E10
https://www.blogger.com/video-play.mp4?contentId=17a32cb0a39e090c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E11
https://www.blogger.com/video-play.mp4?contentId=c3611c95ab78c080&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E12
https://www.blogger.com/video-play.mp4?contentId=6203ff713d3eaa55&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E13
https://www.blogger.com/video-play.mp4?contentId=2cf28333da0100a7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E14
https://www.blogger.com/video-play.mp4?contentId=35c61c4272c57358&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E15
https://www.blogger.com/video-play.mp4?contentId=39b3199a17324645&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E16
https://www.blogger.com/video-play.mp4?contentId=c0032c5a0f4e1c82&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E17
https://www.blogger.com/video-play.mp4?contentId=a4de5b7b38da6605&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E18
https://www.blogger.com/video-play.mp4?contentId=2953b75ec81501b9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E19
https://www.blogger.com/video-play.mp4?contentId=21f26faedbbbca65&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E20
https://www.blogger.com/video-play.mp4?contentId=ea7bda731c6b5b04&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E21
https://www.blogger.com/video-play.mp4?contentId=46959494e64a69f9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E22
https://www.blogger.com/video-play.mp4?contentId=71b7e2036912e934&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ntutdc4kp/supernaturalse11_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T11|E23
https://www.blogger.com/video-play.mp4?contentId=9877b4941395b8b6&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E01
https://www.blogger.com/video-play.mp4?contentId=1f168254b5a41311&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E02
https://www.blogger.com/video-play.mp4?contentId=1dde9d503c5a8a55&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E03
https://www.blogger.com/video-play.mp4?contentId=ae794d307908ffc4&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E04
https://www.blogger.com/video-play.mp4?contentId=23b13dae3659afa9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E05
https://www.blogger.com/video-play.mp4?contentId=53ea52c3ca1f21d0&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E06
https://www.blogger.com/video-play.mp4?contentId=a1679ae6b9de81bf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E07
https://www.blogger.com/video-play.mp4?contentId=29f201757314e7d7&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E08
https://www.blogger.com/video-play.mp4?contentId=b8cd6f88353faa39&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E09
https://www.blogger.com/video-play.mp4?contentId=2127175d39818c99&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E10
https://www.blogger.com/video-play.mp4?contentId=fc7593c40c44790b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E11
https://www.blogger.com/video-play.mp4?contentId=bcb7d7c602a6726&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E12
https://www.blogger.com/video-play.mp4?contentId=21bbe937c624f0a6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E13
https://www.blogger.com/video-play.mp4?contentId=8725c340a2484768&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E14
https://www.blogger.com/video-play.mp4?contentId=a2178baba7c49570&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E15
https://www.blogger.com/video-play.mp4?contentId=9cd702bf39e9c04a&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E16
https://www.blogger.com/video-play.mp4?contentId=5758c76f6f41c20c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E17
https://www.blogger.com/video-play.mp4?contentId=8dcdebacb25d4541&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E18
https://www.blogger.com/video-play.mp4?contentId=ea44423a98c5c3ba&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E19
https://www.blogger.com/video-play.mp4?contentId=6dc5d220ed30ba0c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E20
https://www.blogger.com/video-play.mp4?contentId=3ff69112921fbdd6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E21
https://www.blogger.com/video-play.mp4?contentId=593f14e7b7b6b039&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E22
https://www.blogger.com/video-play.mp4?contentId=6de2092284b0261e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/ytg0oyi55/supernaturalse12_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T12|E23
https://www.blogger.com/video-play.mp4?contentId=1be2a5b4e09686e7&MEGAMAXIPTV


#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E01
https://www.blogger.com/video-play.mp4?contentId=26189994a2762993&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E02
https://www.blogger.com/video-play.mp4?contentId=3db6604cbb01b46&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E03
https://www.blogger.com/video-play.mp4?contentId=32f62a46ebf78417&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E04
https://www.blogger.com/video-play.mp4?contentId=9a92025c72cd747d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E05
https://www.blogger.com/video-play.mp4?contentId=d42f3bf79f3b3edf&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E06
https://www.blogger.com/video-play.mp4?contentId=f64b648829324edb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E07
https://www.blogger.com/video-play.mp4?contentId=f841526d21859a58&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E08
https://www.blogger.com/video-play.mp4?contentId=c0e67f28d0e13b01&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E09
https://www.blogger.com/video-play.mp4?contentId=16e7da49329babe6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E10
https://www.blogger.com/video-play.mp4?contentId=d7769cfabeca28c2&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E11
http://www.blogger.com/video-play.mp4?contentId=5ba2fd8d54c6712e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E12
http://www.blogger.com/video-play.mp4?contentId=791d1eb29d289b93&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E13
http://www.blogger.com/video-play.mp4?contentId=6f4f822b8b286d12&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/focrf7b6x/supernaturalse13_MEGAMAXIPTV.jpg" group-title="Série: SUPERNATURAL",SUPERNATURAL T13|E13
http://www.blogger.com/video-play.mp4?contentId=33ce3aad3cef1a18&MEGAMAXIPTV


################################## Série: AS VISÕES DA RAVEN ##################################

#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E01
http://www.blogger.com/video-play.mp4?contentId=3aeaca7d9f591527&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E02
http://www.blogger.com/video-play.mp4?contentId=c40bf4ae651de7e3&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E03
http://www.blogger.com/video-play.mp4?contentId=a578f905f6d7be31&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E04
http://www.blogger.com/video-play.mp4?contentId=bb2d41e76defaae9&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E05
http://www.blogger.com/video-play.mp4?contentId=d4eab4dced8a3d4c&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E06
http://www.blogger.com/video-play.mp4?contentId=cd59706b322fa159&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E07
http://www.blogger.com/video-play.mp4?contentId=dcec38029d112e1d&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E08
http://www.blogger.com/video-play.mp4?contentId=b9b2a9983d730643&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E09
http://www.blogger.com/video-play.mp4?contentId=2f2706c435d0e5de&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E10
http://www.blogger.com/video-play.mp4?contentId=984625404fc36aae&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E11
http://www.blogger.com/video-play.mp4?contentId=b11f4fb1f6211b72&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E12
http://www.blogger.com/video-play.mp4?contentId=7d27bdc6e4bf5e7e&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E13
http://www.blogger.com/video-play.mp4?contentId=bbedc81b04f10e4f&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E14
http://www.blogger.com/video-play.mp4?contentId=335e1be592a3ea23&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E15
http://www.blogger.com/video-play.mp4?contentId=ebddcbd603907ed&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E16
http://www.blogger.com/video-play.mp4?contentId=183ad65056a406cb&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E17
http://www.blogger.com/video-play.mp4?contentId=3b17403092fd299b&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E18
http://www.blogger.com/video-play.mp4?contentId=6e1f86fe5a3003e6&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E19
http://www.blogger.com/video-play.mp4?contentId=31c827b6c8c98b35&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E20
http://www.blogger.com/video-play.mp4?contentId=16b5eebeff36f663&MEGAMAXIPTV
#EXTINF:-1 tvg-id="" tvg-name="" tvg-logo="https://s26.postimg.org/x7lmti97t/asvisoesdaravenMEGAMAXIPTV.jpg" group-title="Série: AS VISÕES DA RAVEN",AS VISÕES DA RAVEN T01|E21
http://www.blogger.com/video-play.mp4?contentId=ac49d56d75b98452&MEGAMAXIPTV